//Variables
nomOk = false;
champsOk = false;
let newP = document.createElement("p");

//Verification des champs obligatoires
$("#btnSave").click(function(){
    if (($("#nomArtiste").val() == "") || ($("#selectionRole").val() == "0") || ($("#photo").val() == "" ))  {
        //alert ("Vous devez remplir tous les champs obligatoires!")
        champsOk = false;
     }
     else{
         champsOk = true;
     }
});


//Nom de l'artiste est obligatoirement un champ alphabetique
let nomDartiste = document.getElementById("nomArtiste");
let BoutonPrincipale = document.getElementById("btnSave");
let nomDartisteRegEx = /^[a-z A-Z]+$/

BoutonPrincipale.addEventListener("click", function(){
    if (nomDartisteRegEx.test(nomDartiste.value) == false || nomDartiste.value ==""){
        $("#messageNom").text("Le nom d'artiste ne peut seulement contenir des caractères alphabétiques et est obligatoire !");
        nomOk = false;
    }
    else{
        $("#messageNom").text("");
        nomOk = true;
    }

});

// Role obligatoire
BoutonPrincipale.addEventListener("click", function(){
    if ($("#selectionRole").val() == 0){
        $("#messageRole").text("Le role est obligatoire");
        //nomOk = false;
    }
    else{
        $("#messageRole").text("");
        //nomOk = true;
    }
});

//Lien photo obligatoire
BoutonPrincipale.addEventListener("click", function(){
    if ($("#photo").val() == ""){
        $("#messagePhoto").text("La selection d'une photo est obligatoire");
        //nomOk = false;
    }
    else{
        $("#messagePhoto").text("");
        //nomOk = true;
    }
});


//Verification en temps reel si les champs sont correcte ***Ajouter a pour faire fonctionner correctement le php avec 
let verifNomTempsReel = false;
let verifChampsTempsReel = false;

function verifTempsReel(){
    if ((nomDartisteRegEx.test(nomDartiste.value) == false || nomDartiste.value =="") || ($("#messageNomServer").innerHTML == "")){
        verifNomTempsReel = false;
    }
    else{
        verifNomTempsReel = true;
    }

    if($("#nomArtiste").val() != "") {
        verifChampsTempsReel = true;
    }
    else{
        verifChampsTempsReel = false;
    }

    if($("#selectionRole").val() != "0") {
        verifChampsTempsReel = true;
    }
    else{
        verifChampsTempsReel = false;
    }

    if($("#photo").val() != "") {
        verifChampsTempsReel = true;
    }
    else{
        verifChampsTempsReel = false;
    }
}
$("#nomArtiste").on('input', function(){
    verifTempsReel();
 });
 $("#selectionRole").on('input', function(){
     verifTempsReel();
 })
 $("#photo").on('input', function(){
     verifTempsReel();
 })
 $("#pays").on('input', function(){
     verifTempsReel();
 })

//Fonction pour valider le bouton submit
function ValidationFinale(){
    if (verifChampsTempsReel == false || verifNomTempsReel == false){
        return false;
    }
    else{
        return true;
    }
}



//Evenement de confirmation apres les verifications
BoutonPrincipale.addEventListener("click", function(){
    if (nomOk == true && champsOk == true){
        $("#btnSave").attr("data-toggle","modal");
        $("#btnSave").attr("data-target","#myModal");

        $("#p1").text("Nom de l'artiste: " + nomDartiste.value);
        $("#p2").text("Role de l'artiste: " + $("#selectionRole").val());
        $("#p3").text("Pays d'origine: " +  $("#pays").val());
        $("#p4").text("Photo: " +  $("#photo").val());    
    }
});

BoutonPrincipale.addEventListener("click", function(){
    if (nomOk == false || champsOk == false){
        $("#btnSave").removeAttr("data-toggle");
        $("#btnSave").removeAttr("data-target");    
    }
});


