<?php
    require_once('../Includes/entete.php');
?>


<!-- Titre principale -->
<div id="corps">
    
    <div id="titre" class="container-md">
        <div class="row">
            <div class="col-12 mt-4 text-center text-primary">
                <h2>Ajouter un artiste</h2>
            </div>
        </div>
    </div>

<div class="container-sm mainContainer">

    <div class="row">
    <div class="col-lg-3 col-sm-0"></div>

<!--------------- Validation cote serveur ---------------------------->
<?php
//Verification et validation du formulaire avant d'ajouter un artiste a la BD
$erreurNom = "";

    if(isset($_POST['Enregistrer'])){
        if($_POST['Enregistrer'] == "IsEnregistrer"){
            $valide = true;

            if($valide == true && !empty($_POST['nom'])){
                $nom = ValiderEntree($_POST['nom']);
                if(VerifierArtiste($nom)){
                    $valide = false;
                    $erreurNom = "L'artiste existe déjà dans la bibliothèque.";
                }
                if(strlen($nom) < 2){
                    $valide = false;
                    $erreurNom = "Le nom de l'artiste doit contenir plus d'un caractère.";
                }
            }

            if($valide == true && !empty($_POST['role'])){
                $role = $_POST['role'];
            }
                else
                {
                    $valide = false;
                }
            if($valide == true && !empty($_POST['photo'])){ 
                $photoArtiste = $_POST['photo'];
            }
                else
                {
                    $valide = false;
                }

            if($valide == true && isset($_POST['pays'])){
                $pays = $_POST['pays']; 

            }
                else
                {
                    $valide = false;
                }

            if($valide == true ){
                AjouterArtiste($nom, $role, $pays, $photoArtiste);
            }
        }
    }
?>


        <!-- FORM -->
        <div class="col-lg-6 col-sm-12" id="formContainer">
            <form method="POST" enctype="multipart/form-data" action="" id="formulaireArtiste">
                
                <div class="form-row">
                    <div class="col-12">
                        <label class="form-control-label" for="Nom">Nom *</label>
                        <input class="form-control" type="text" name="nom" id="nomArtiste"> 
                    </div>
                    <div id="messageNom" class="col-12"></div>
                    <div id="messageNomServer" class = "col-12"><?php if ($erreurNom != "") {echo($erreurNom);}?> </div>
                </div>      
                
                <div class="form-row">
                    <div class="col-12">
                        <label for="role">Rôle *</label>
                        <select id="selectionRole" name = "role"> 
                            <option selected value="0">Choisir un rôle</option>
                            <option value="chanteur">Chanteur</option>
                            <option value="compositeur">Compositeur</option>
                            <option value="interprète">Interprète</option>
                            <option value="auteur">Auteur</option>
                        </select> 
                    </div>
                    <div id="messageRole" class="col"></div>
                </div>

                <div class="form-row" id="divPays">
                    <div class="col">
                        <label for="pays">Le pays d'origine</label>
                        <input type="text" class="form-control" name="pays" id="pays"> 
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-12">
                        <label for="photo">Lien photo *</label>
                        <input type="text" class="form-control" name="photo" id="photo"> 
                    </div>
                    <div id="messagePhoto" id= ""></div>
                </div>

                <button onclick="return ValidationFinale()" type = "submit" class="btn btn-primary" id="btnSave" name="Enregistrer" Value="IsEnregistrer">Enregistrer</button>

                <p id="etoile">* Champs obligatoires</p>
            </form>
        </div>

    <div class="col-lg-3 col-sm-0"></div>
    </div> 
    <!------------------------ Fin du form *** ----------------------->
</div>


<!-- Fenetre Modale -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Contenu fenetre-->
        <div class="modal-content">

            <div class="modal-header">
            <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
            <h4 class="" id="titreFenetre">Artiste Ajouté avec succès!</h4>
            </div>

            <ul id="textConfirmation">
                <li id="p1"></li>
                <li id="p2"></li>
                <li id="p3"></li>
                <li id="p4"></li>
            </ul>

        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal" id="fermerModale">Fermer</button>
        </div>
    </div>

</div>

</div>



<!-- //Inclusion du footer de page  -->

<?php require_once('../Includes/pied.php'); ?>