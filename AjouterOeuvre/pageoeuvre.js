$(document).ready(function () {

    //Mettre la date d'aujourd'hui dans le input Date d'ajout
    dateAujourdhui = $('#dateToday').val(new Date().toLocaleDateString("fr-CA"));

    //Mise en veille des messages d'erreur
    $('#fichierCheck').hide();
    $('#dureeCheck').hide();
    $('#artisteCheck').hide();
    $('#titreCheck').hide();
    $('#parolesCheck').hide();
    $('#albumCheck').hide();

    //Reagir au changements
    $('#titre').change(function (){
        validerTitre()
    });
    $('#duree').change(function (){
        validerDuree()
    });
    $('#fichier').change(function(){
        validerFichier()
    });

    //Reagir au changement du select
    $('#artiste').change(function (){
        validerArtiste()
    });
    $('#album').change(function(){
        validerAlbum()
    });
});

//Recuperer ID
var artisteValide = $('#artiste');
var albumValide = $('#album');
var titreValide = $('#titre');

function validerFichier(){
    let fichierValide = $('#fichier').val();
    var regex = /^\d*[.,]?\d*$/;
    
    if (!regex.test(fichierValide)){
        $('#fichierCheck').show();
        return false;
    }
    else{
        $('#fichierCheck').hide();
        return true;
    }
}

function validerDuree(){
    let dureeValide = $('#duree').val();
    var regex = /^\d*$/g;;

    if(dureeValide == ''){
        $('#dureeCheck').show();
        return false;
    }
    else if (!regex.test(dureeValide)){
        $('#dureeCheck').show();
        $('#dureeCheck').html("Doit contenir seulement un nombre entier positif.");
        return false;
    }
    else{
        $('#dureeCheck').hide();
        return true;
    }
}

function validerArtiste() {

    var inputValide = artisteValide.find(":selected");
    $('#artisteCheck').html("**Ce champ est requis**");

    if ($.type(inputValide.text()) == "string")
    {
        if (inputValide.val() == -1) {
            $('#artisteCheck').show();
            return false;
        }
        else {
            $('#artisteCheck').hide();
            return true;
        }
    }
    else
    {
        $('#artisteCheck').html("Le nom de l'artiste doit être une chaîne de caractère.");
        $('#artisteCheck').show();
        return false;
    }
}

function validerTitre() {
    
    var inputValide = titreValide.val();

    if ($.type('#titre') == "string")
    {
        if (inputValide == "") {
            $('#titreCheck').html("**Ce champ est requis**");
            $('#titreCheck').show();
            return false;
        } 
        else {
            $('#titreCheck').hide();
            return true;
        }
    }
    else
    {
        $('#titreCheck').html("Le titre doit être une chaîne de caractère.");
        $('#titreCheck').show();
        return false;
    }
}

function validerParoles() {
    
    var inputValide = $('#paroles');

    if ($.type('#paroles') == "string")
    {
        return true;
    }
    else
    {
        $('#parolesCheck').show();
        return false;
    }
}

function validerAlbum() {
    var inputValide = albumValide.find(":selected");
    $('#albumCheck').html("**Ce champ est requis**");

    if ($.type(inputValide.text()) == "string")
    {
        if (inputValide.val() == -1) {
            $('#albumCheck').show();
            return false;
        }
        else {
            $('#albumCheck').hide();
            return true;
        }
    }
    else
    {
        $('#albumCheck').html("Le nom de l'album doit être une chaîne de caractère.");
        $('#albumCheck').show();
        return false;
    }
}

function validerEnregistrer() {

    var title = $("h6");
    
    if(validerArtiste() == true && validerDuree() == true && validerTitre() == true && validerFichier() == true && validerAlbum() == true && validerParoles() == true)
    {
        title.html("");
        return true;
    }
    else
    {
        validerFichier();
        validerArtiste();
        validerDuree();
        validerTitre();
        validerAlbum();
        validerParoles();
        title.css("color", "red");
        return false;
    }

}
