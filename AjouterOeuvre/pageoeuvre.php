<?php  require_once('../Includes/entete.php'); ?>

<?php

    
 if(isset($_POST['submit'])){
    if($_POST['submit'] == "Submit"){

        $valide = true;

        if($valide == true && !empty($_POST['titre'])){
            $titre = ValiderEntree($_POST['titre']);
        }
        else
        {
            $valide = false;
        } 
        if($valide == true && !strpos($_POST['artiste'], "Choisir un artiste")){
            $artiste = $_POST['artiste'];
        }
        else
        {
            $valide = false;
        }

        
        if($valide == true && !empty($_POST['duree'])){ 
            $duree = $_POST['duree'];
        }
        else
        {
            $valide = false;
        }

        if($valide == true && isset($_POST['tailleMB'])){ 
            $taillemb = $_POST['tailleMB'];
        }

        if($valide == true && isset($_POST['paroles'])){
            $lyrics = $_POST['paroles']; 

        }

        if($valide == true && !empty($_POST['date'])){ 
            $date = $_POST['date'];
        }
        else
        {
            $valide = false;
        }

        if($valide == true && !empty($_POST['album'])){
            $album = $_POST['album'];
        }
        else
        {
            $valide = false;
        }

        if($valide == true){
            $_SESSION['formSoumise'] = true;
            $id_artiste = TrouverIdArtiste($artiste);
            $id_album = TrouverIdAlbum($album);
            AjouterOeuvre($titre, $id_artiste, $duree, $taillemb, $lyrics, $date, $id_album);
        }
    }
}

?>

<div id="corps">
    <div class="container-md titrePrincipale text-primary">
        <div class="row">
            <div class="col-12">
                <h1>Ajouter une oeuvre</h1>
                <h6>Veuillez remplir les champs ci-dessous.</h6>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title text-primary" id="exampleModalLongTitle">Oeuvre ajouté:</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-md-3 mt-5" id="albumFakeCover">
                            </div>
                            <div class="col-md-8 pt-3">
                                <div class="row">
                                    <div class="col-md-6 col-sm-4">
                                        <p class="text-secondary">Titre:</p>
                                    </div>
                                    <div class="col-md-6 col-sm-7">
                                        <?php
                                            echo "<p class='font-weight-bold text-dark'>".$titre."</p>";
                                        ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-4">
                                        <p class="text-secondary">Artiste:</p>
                                    </div>
                                    <div class="col-md-6 col-sm-7">
                                        <?php
                                            echo "<p class='font-weight-bold text-dark'>".$artiste."</p>";
                                        ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-4">
                                        <p class="text-secondary">Durée (sec):</p>
                                    </div>
                                    <div class="col-md-6 col-sm-7">
                                        <?php
                                            echo "<p class='font-weight-bold text-dark'>".$duree."</p>";
                                        ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-4">
                                        <p class="text-secondary">Taille (Mb):</p>
                                    </div>
                                    <div class="col-md-6 col-sm-7">
                                        <?php
                                            echo "<p class='font-weight-bold text-dark'>".$taillemb."</p>";
                                        ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-4">
                                        <p class="text-secondary">Paroles:</p>
                                    </div>
                                    <div class="col-md-6 col-sm-7">
                                        <?php
                                            echo "<p class='font-weight-bold text-dark'>".$lyrics."</p>";
                                        ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-4">
                                        <p class="text-secondary">Date:</p>
                                    </div>
                                    <div class="col-md-6 col-sm-7">
                                        <?php
                                            echo "<p class='font-weight-bold text-dark'>".$date."</p>";
                                        ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-4">
                                        <p class="text-secondary">Nom de l'album:</p>
                                    </div>
                                    <div class="col-md-6 col-sm-7">
                                        <?php
                                            echo "<p class='font-weight-bold text-dark'>".$album."</p>";
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="btnOk" class="btn btn-primary" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mainContainer container" id="backgroundedContainer">
        <div class="row justify-content-center">
                <div class="col-lg-6 col-md-8" id="formContainer">
                    <form method="POST" enctype="multipart/form-data"
                        action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">

                        <div class="row form-group">
                            <div class="col">
                                <label for="titre" class="form-control-label">Titre*</label>
                                    <input type="text" id="titre" name="titre" class="form-control">
                                <h5 class="erreur" id="titreCheck" style="color: red;">
                                    **Ce champ est requis**
                                </h5>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <label for="artiste" class="form-control-label">Artiste*</label>
                                <select id="artiste" name="artiste" class="custom-select custom-select-sm">
                                    <?php
                                        $artiste = 0;
                                        ListerChanteur($artiste);
                                    ?>
                                </select>
                                <h5 class="erreur" id="artisteCheck" style="color: red;">
                                    **Ce champ est requis**
                                </h5>
                            </div>
                        </div>

                        <div class="form-group form-row mt-3">
                            <div class="col">
                                <label for="duree" class="form-control-label">Durée* (En secondes)</label>
                                <input type="text" class="inputForm" name="duree" id="duree">
                                <h5 class="erreur" id="dureeCheck" style="color: red;">
                                    **Ce champ est requis**
                                </h5>
                            </div>
                        </div>

                        <div class="form-group form-row">
                            <div class="col">
                                <label for="tailleMB" class="form-control-label">Tailles du fichier (Mb)</label>
                                <input type="text" class="inputForm" name="tailleMB" id="fichier">
                                <h5 class="erreur" id="fichierCheck" style="color: red;">
                                    Doit contenir seulement des chiffres
                                </h5>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <label for="paroles" class="form-control-label">Paroles</label>
                                <input type="text" class="form-control" name="paroles" id="paroles">
                                <h5 class="erreur" id="parolesCheck" style="color: red;">
                                    Les paroles doivent être de type texte.
                                </h5>
                            </div>
                        </div>
                        <div class="form-row ">
                            <div class="col">
                                <label class="form-control-label" for="dateAjout">Date d'ajout*</label>
                                <input type="date" class="form-control" name="date" id="dateToday">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <label for="album" class="form-control-label">Nom de l'album*</label>
                                <select class="custom-select custom-select-sm" name="album" id="album">
                                    <?php
                                        $album = 0;
                                        ListerAlbum($album);
                                    ?>

                                </select>
                                <h5 class="erreur" id="albumCheck" style="color: red;">
                                    **Ce champ est requis**
                                </h5>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary" id="btnSave" name="submit" value='Submit'
                            onclick="return validerEnregistrer()">Enregistrer</button>
                            <small class="text-secondary mb-1">* Champs obligatoires</small>
                        </form>

                </div>
            
        </div>
</div>

<?php
require_once('../Includes/pied.php');
?>