
//Input objects
let formContainer = document.getElementById("formContainer");

let formNomAlbum = document.getElementById("nomAlbum");
let formDateCreation = document.getElementById("dateCreation");
let formGenre = document.getElementById("genre");
let formCouverture = document.getElementById("couverture");

let date = new Date();

//Date constructor
function TodayDate(year, month, day) {

    this.year = (date.getFullYear()).toString();

    if((date.getMonth()).toString().length == 1)
    {
        this.month = "0" + (date.getMonth()).toString();
    }
    else
    {
        this.month = (date.getMonth()).toString();
    }
    if((date.getDate()).toString().length == 1)
    {
        this.day = "0" + (date.getDate()).toString();
    }
    else
    {
        this.day = (date.getDate()).toString();
    }
}

let todayDate = new TodayDate();

//Event listeners
formNomAlbum.addEventListener("change", ValidationNomAlbum);
formGenre.addEventListener("change", ValidationGenre);
formCouverture.addEventListener("change", ValidationCouverture);

//Functions
function ValidationNomAlbum(){

    var maskNomAlbum = /^[a-zA-Z\s]*$/g;
    var inputNomAlbum = formNomAlbum.value.trim();
    var alertNomAlbum = document.getElementById("alertNomAlbum");

    if(inputNomAlbum == "")
    {
        formNomAlbum.classList.add("is-invalid");
        alertNomAlbum.innerHTML = "Vous devez entrer un nom.";
        SetHeightContainers();
        return false;
    }
    else if(!maskNomAlbum.test(inputNomAlbum))
    {
        formNomAlbum.classList.add("is-invalid");
        alertNomAlbum.innerHTML = "Le nom doit contenir uniquement des lettres.";
        SetHeightContainers();
        return false;
    }
    else
    {
        formNomAlbum.classList.remove("is-invalid");
        alertNomAlbum.innerHTML = "";
        SetHeightContainers();
        return true;
    }
}

function ValidationGenre(){

    var inputSelect = formGenre;
    var inputGenre = inputSelect.selectedIndex;
    var alertGenre = document.getElementById("alertGenre");

    if(inputGenre == 0)
    {
        formGenre.classList.add("is-invalid");
        alertGenre.innerHTML = "Vous devez choisir un genre musical.";
        SetHeightContainers();
        return false;
    }
    else
    {
        formGenre.classList.remove("is-invalid");
        alertGenre.innerHTML = "";
        SetHeightContainers();
        return true;
    }
}

function ValidationCouverture(){

    var inputCouverture = formCouverture.value;
    var alertCouverture = document.getElementById("alertCouverture");
    var maskCouverture = /^.*\.(jpg|JPG|gif|GIF|png|PNG|jpeg|JPEG|bmp|BMP|svg|SVG)$/g;

    if(inputCouverture == "")
    {
        formCouverture.classList.add("is-invalid");
        alertCouverture.innerHTML = "Vous devez choisir une couverture pour l'album."
        SetHeightContainers();
        return false;
    }
    else if(!maskCouverture.test(inputCouverture))
    {
        formCouverture.classList.add("is-invalid");
        alertCouverture.innerHTML = "Les types de fichiers acceptés sont jpeg, jpg, png, gif, bmp et svg";
        SetHeightContainers();
        return false;
    }
    else
    {
        formCouverture.classList.remove("is-invalid");
        alertCouverture.innerHTML = "";
        SetHeightContainers();
        return true;
    }
}

function Validation(){

    var title = document.getElementsByTagName("h6")[0];

    if(!formDateCreation.value){

        formDateCreation.value = todayDate.year + "-" + todayDate.month + "-" + todayDate.day;
    }

    if(ValidationNomAlbum() && ValidationGenre() && ValidationCouverture())
    {
        title.innerHTML = "";
        return true;
    }
    else
    {
        ValidationNomAlbum();
        ValidationGenre();
        ValidationCouverture();

        title.style.color = "red";
        return false;
    }
}

function SetHeightContainers () {

    var formContainerPosition = formContainer.getBoundingClientRect().bottom;
    var styleFormContainer = window.getComputedStyle(formContainer);
    var heightFormContainer = parseInt((styleFormContainer.height).substring(0,3));

    var button = document.getElementById("btnSave");
    var buttonPosition = button.getBoundingClientRect().bottom;

    var buttonSpacingBottomForm = formContainerPosition - buttonPosition;
    var spaceToAdd = 64 - buttonSpacingBottomForm;

    var mainContainer = document.getElementsByClassName("mainContainer")[0];

    formContainer.style.height = (heightFormContainer + spaceToAdd).toString() + "px";
    mainContainer.style.height = (heightFormContainer + 200 + spaceToAdd).toString() + "px";
}
