<?php
    require_once('../Includes/entete.php');

    $erreurNom = "";

    if(isset($_POST['enregistrer'])){
        if($_POST['enregistrer'] == "Enregistrer"){
            $valide = true;
            if($valide && isset($_POST['nom'])){
                $nom = ValiderEntree($_POST['nom']);
                if(VerifierAlbum($nom)){
                    $valide = false;
                    $erreurNom = "L'album existe déjà dans la bibliothèque.";
                }
                if(strlen($nom) < 2){
                    $valide = false;
                    $erreurNom = "Le nom de l'album doit contenir plus d'un caractère.";
                }
            }
            if($valide && isset($_POST['date'])){
                $date = $_POST['date'];
            }
            else
            {
                $valide = false;
            }
            if($valide && isset($_POST['genre'])){
                $genre = $_POST['genre'];
            }
            else
            {
                $valide = false;
            }
            if($valide && isset($_FILES['couverture'])){
                $pht_couvt = $_FILES['couverture']['name'];
            }
            else
            {
                $valide = false;
            }
            if($valide){
                $_SESSION['formSoumise'] = true;
                TelechargerCouverture();
                $id_genre = TrouverIdGenre($genre);
                AjouterAlbum($nom, $date, $id_genre, $pht_couvt);
            }
        }
    }
?>
        <div id="corps">
            <div id="titre" class="container-md">
                <div class="row">
                    <div class="col-12 mt-4 text-center text-primary">
                        <h1>Ajouter un album</h1>
                        <h6>Veuillez remplir les champs ci-dessous.</h6>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title text-primary" id="exampleModalLongTitle">Album ajouté:</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="container-fluid">
                                <div class="row justify-content-center">
                                        <div class="col-md-3" id="albumFakeCover">
                                        </div>
                                        <div class="col-md-8 pt-3">
                                            <div class="row"><div class="col-md-6 col-sm-4">
                                                <p class="text-secondary">Nom de l'album:</p>
                                            </div>
                                            <div class="col-md-6 col-sm-7">
                                                <?php
                                                    echo "<p class='font-weight-bold text-dark'>".$nom."</p>";
                                                ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-4">
                                                <p class="text-secondary">Date de création:</p>
                                            </div>
                                            <div class="col-md-6 col-sm-7">
                                                <?php
                                                    echo "<p class='font-weight-bold text-dark'>".$date."</p>";
                                                ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-4">
                                                <p class="text-secondary">Genre:</p>
                                            </div>
                                            <div class="col-md-6 col-sm-7">
                                                <?php
                                                    echo "<p class='font-weight-bold text-dark'>".$genre."</p>";
                                                ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-4">
                                                <p class="text-secondary">Couverture:</p>
                                            </div>
                                            <div class="col-md-6 col-sm-7">
                                                <?php
                                                    echo "<p class='font-weight-bold text-dark'>".$pht_couvt."</p>";
                                                ?>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnOk" class="btn btn-primary" data-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>
</div>
            <div class="mainContainer container" id="backgroundedContainer">
                <div class="row justify-content-center">
                    <div class="col-lg-6 col-md-8" id="formContainer"> 
                        <form method="POST" enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                            <div class="row form-group">
                                <label class="col-md-5 col-form-label text-secondary" for="nomAlbum">Nom de l'album: <small>*</small></label>
                                <div class="col-md-7">
                                    <input type="text" id="nomAlbum" name="nom" class="form-control">
                                </div>
                            </div>
                            <p class="text-danger align-self-center" id="alertNomAlbum"></p>
<?php
    if($erreurNom != ""){
        echo "<p class='text-danger align-self-center' id='alertNomAlbum'>".$erreurNom."</p>";
    }
?>
                            <div class="row form-group">
                                <label class="col-md-5 col-form-label text-secondary" for="dateCreation">Date de création: <small>*</small></label>
                                <div class="col-md-5">
                                    <input type="date" id="dateCreation" name="date" class="form-control">
                                </div>
                            </div>
                            <p class="text-danger align-self-center" id="alertDateCreation"></p>
                            <div class="row form-group">
                                <label class="col-md-5 col-form-label text-secondary" for="genre">Genre: <small>*</small></label>
                                <div class="col-md-5">
                                    <select id="genre" name="genre" class="custom-select custom-select-sm">
<?php
    $genres = 0;
    ListerGenres($genres);
?>
                                    </select>
                                </div>
                            </div>
                            <p class="text-danger align-self-center" id="alertGenre"></p>
                            <div class="row form-group">
                                <label class="col-md-5 col-form-label text-secondary" for="couverture">Couverture: <small>*</small></label>
                                <div class="custom-file col-md-6">
                                    <input type="file" class="custom-file-input" id="couverture" name="couverture">
                                    <label class="custom-file-label" for="couverture">Choisir un fichier</label>
                                </div>
                            </div>
                            <p class="text-danger align-self-center" id="alertCouverture"></p>
                            <button type="submit" class="btn btn-primary" id="btnSave" name="enregistrer" value="Enregistrer" onclick="return Validation()">Enregistrer</button>
                            <small class="text-secondary mb-1">* Champs obligatoires</small>
                        </form><!------------------------ Fin du form *** ----------------------->
                    </div>

                </div>             
            </div>
        </div>
<?php
    require_once('../Includes/pied.php');
?>
