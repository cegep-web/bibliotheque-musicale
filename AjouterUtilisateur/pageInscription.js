//Variables
mdpOk = false;
courrielOk = false;
ageOk = false;
champsOk = false;

//Fonction de verification de champs obligatoires
function verificationChamps(){
    if (($("#nomUtilisateur").val() == "")||($("addresseCourriel").val() =="" ) || ($("#mdp").val() == "0") || ($("#mdpConf").val() == "" ))  {
        $("#messageChamps").text("Vous devez remplir tous les champs obligatoires!")
        champsOk = false;
     }
     else{
         champsOk = true;
         $("#messageChamps").text("")
     }
}
//Verification des champs obligatoires sur le click
$("#btnSave").click(function(){
    verificationChamps();
});


//Verification du format du champ courriel avec RegEx
let addCourriel = document.getElementById("addresseCourriel");
let BoutonPrincipale = document.getElementById("btnSave");
let addCourrielRegEx = /[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/;

$("#addresseCourriel").on("input",(function(){
    verificationChamps();
    if (addCourrielRegEx.test(addresseCourriel.value) == true){
        courrielOk = true;
        $("#messageCourriel").text("");
    }
    else if (addCourrielRegEx.test(addresseCourriel.value) == false){
        courrielOk = false;
        $("#btnSave").removeAttr("data-toggle");
        $("#btnSave").removeAttr("data-target");
        $("#messageCourriel").text("L'addresse courriel entré n'est pas valide...");
    }
}))


//Verification du champ de mot de passe avec RegEx
let motDePasse = document.getElementById("mdp");
let mdpRegEx = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).{8,}/;

$("#mdp").on("input",(function(){
    verificationChamps();
        if (mdpRegEx.test(motDePasse.value) == false){
            $("#message").text("Invalide");
            $("#btnSave").removeAttr("data-toggle");
            $("#btnSave").removeAttr("data-target");
            mdpOk = false;
        }
        else if (mdpRegEx.test(motDePasse.value) == true){
            $("#message").text("");
            mdpOk = true;
        }
    })
);


//Verification de l'age
$("#age").on("input", (function(){
    verificationChamps();
    if($("#age").val() == ""){
        console.log("vide")
        ageOk = true;
    }
    else if ($("#age").val() < 18 || $("#age").val() > 100 ){
        ageOk = false;
        $("#btnSave").removeAttr("data-toggle");
        $("#btnSave").removeAttr("data-target");
        $("#messageAge").text("Vous devez entrer une âge valide s'il-vous-plaît (entre 18 et 100 ans)")
    }
    else if ($("#age").val() >= 18 && $("#age").val() <= 100 ){
        $("#messageAge").text("")
        ageOk = true;
    }
})
);


//Verification de la confirmation des deux mots de passes
let motDePasseConf = document.getElementById("mdpConf");
$("#mdp").on("input",(function(){
     verificationChamps();
        if (motDePasse.value != motDePasseConf.value || motDePasseConf.value != motDePasse.value){
            $("#message2").text("Mot de passe non identique");
            $("#btnSave").removeAttr("data-toggle");
            $("#btnSave").removeAttr("data-target");
            mdpOk = false;
        }
        else if (motDePasse.value == motDePasseConf.value || motDePasseConf.value == motDePasse.value){
            mdpOk = true;
            $("#message2").text("");
        }
    })
);
    $("#mdpConf").on("input",(function(){
     verificationChamps();
        if (motDePasse.value != motDePasseConf.value || motDePasseConf.value != motDePasse.value){
            $("#message2").text("Mot de passe non identique");
            $("#btnSave").removeAttr("data-toggle");
            $("#btnSave").removeAttr("data-target");
            mdpOk = false;
        }
        else if (motDePasse.value == motDePasseConf.value || motDePasseConf.value == motDePasse.value){
            $("#message2").text("");
            mdpOk = true;
        }
    })
);

//Verification pour le server avant l'envoi du formulaire avec un OnClick sur le bouton de soumission
function VerifServer(){
    if (mdpOk == true && courrielOk == true && champsOk == true)  {
        console.log("returned true")
        return true;

    }
    else{
        console.log("returned false")
        return false;
    }
}


//Verification Finale de toutes les conditions
$("#btnSave").click(function(){
    if (mdpOk ==true && courrielOk == true && ageOk == true && champsOk == true)  {
        $("#btnSave").attr("data-toggle","modal");
        $("#btnSave").attr("data-target","#myModal");

        $("#p1").text("Nom d'utilisateur: " + $("#nomUtilisateur").val());
        $("#p2").text("Addresse courriel: " + $("#addresseCourriel").val());
        $("#p3").text("Âge: " +  $("#age").val());   

        console.log("Utilisateur creer avec succes");
     }

     else if (mdpOk ==false || courrielOk == false || ageOk == false || champsOk == false){
        $("#btnSave").removeAttr("data-toggle");
        $("#btnSave").removeAttr("data-target");

     }
});


