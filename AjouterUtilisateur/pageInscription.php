<?php
    require_once('../Includes/entete.php');
?>

<!--------------- Validation cote serveur ---------------------------->
<?php
//Verification et validation du formulaire avant d'ajouter un artiste a la BD (Courriel unique)
$erreurCourriel = "";

    if(isset($_POST['Enregistrer'])){
        if($_POST['Enregistrer'] == "IsEnregistrer"){
            $valide = true;

            if($valide == true && !empty($_POST['courriel'])){
                $courriel = ValiderEntree($_POST['courriel']);
                if(VerifierCourriel($courriel)){
                    $valide = false;
                    $erreurCourriel = "Ce courriel est déjà utilisé par un autre utilisateur.";
                }
                else{
                    $erreurCourriel = "";
                }
            }

            if($valide == true && !empty($_POST['username'])){
                $nom = $_POST['username'];
            }
                else
                {
                    $valide = false;
                }
            if($valide == true && !empty($_POST['password'])){ 
                $motDePasse = $_POST['password'];
            }
                else
                {
                    $valide = false;
                }

            if($valide == true && isset($_POST['age'])){
                $age = $_POST['age']; 
            }


            if($valide == true ){
                if (empty($age)){
                    AjouterUtilisateur($nom, $courriel, $motDePasse, null);
                    // echo("ajouter avec succes (sans age)");
                }
                else if (isset($age)){
                    AjouterUtilisateur($nom, $courriel, $motDePasse, $age);
                    // echo("ajouter avec succes (avec age)");
                }
            }
        }
    }
?>

<div id="corps">
    <div id="titre" class="container-md">
        <div class="row">
            <div class="col-12 mt-4 text-center text-primary">
                <h2>Ajouter Un Utilisateur</h2>
            </div>
        </div>
    </div>


<div class="containter-sm mainContainer">

<div class="row">
    <div class="col-lg-3 col-sm-0"></div>

    <div class="col-lg-6 col-sm-12" id="formContainer"> 
        <form method="POST" enctype="multipart/form-data" action="">    

                <div class="form-row">
                    <div class="col">
                        <label for="username">Nom d'utilisateur *</label>
                        <input type="text" class="form-control" name="username" id="nomUtilisateur"> 
                    </div>
                </div>

                <div class="form-row">
                    <div class="col">
                        <label for="courriel">Courriel *</label>
                        <input type="email" class="form-control" name="courriel" id="addresseCourriel"> 
                        <div id="messageNomServer" class = "col-12"><?php if ($erreurCourriel != "") {echo($erreurCourriel);}?> </div>
                        <p id="messageCourriel"></p>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col">
                        <label for="password">Mot de Passe *</label>
                        <p id="message"></p>
                        <input type="password" class="form-control" name="password" id="mdp"> 
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <label for="passwordConf">Confirmez votre Mot de Passe *</label>
                        <p id="message2" class="message"></p>
                        <input type="password" class="form-control" name="passwordConf" id="mdpConf"> 
                    </div>
                </div>

                <div class="form-row">
                    <div class="col">
                        <label for="age">Âge</label>
                        <input type="text" class="form-control" name="age" id="age"> 
                        <p id="messageAge"></p>
                    </div>
                </div>

                <div>
                    <p id="messageChamps"></p>
                </div>

                <button type = "submit" class="btn btn-primary" id="btnSave" name="Enregistrer" Value="IsEnregistrer" onclick="return VerifServer()" >Enregistrer</button>

                <p id="etoile">* Champs obligatoires</p>
        </form>

    </div>

    <div class="col-lg-3 col-sm-0"></div>
</div> 
<!------------------------ Fin du form *** ----------------------->
</div>

<!-- Fenetre -->
<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog">
<!-- Contenu fenetre-->
<div class="modal-content">

        <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="" id="titreFenetre">Utilisateur Ajouté avec succès!</h4>
        </div>

    <ul id="textConfirmation">
        <li id="p1"></li>
        <li id="p2"></li>
        <li id="p3"></li>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="fermerModale">Fermer</button>
    </div>

</div>

</div>
</div>



<!-- //Inclusion du footer de page  -->
<?php require_once('../Includes/pied.php'); ?>