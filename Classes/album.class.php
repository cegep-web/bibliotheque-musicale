<?php
    require_once('oeuvre.class.php');

    class Album
    {
    //Atributs
        private $m_id_album;
        private $m_nom;
        private $m_nom_artiste;
        private $m_date_album;
        private $m_genre;
        private $m_oeuvres;
        private $m_pht_couvt;
    //Constructeur
        public function __construct($id_album, $nom, $date_album, $genre, $pht_couvt){
            $this->m_id_album = $id_album;
            $this->m_nom = $nom;
            $this->m_date_album = $date_album;
            $this->m_genre = $genre;
            $this->m_oeuvres = array();
            $this->m_pht_couvt = $pht_couvt;
        }
    // Getter/Setter
        public function getIdAlbum(){
            return $this->m_id_album;
        }
        public function setIdAlbum($p_id_album){
            $this->m_id_album = $p_id_album;
        }
        public function getNom(){
            return $this->m_nom;
        }
        public function setNom($p_nom){
            $this->m_nom = $p_nom;
        }
        public function getNomArtiste(){
            return $this->m_nom_artiste;
        }
        public function setNomArtiste($p_nom_artiste){
            $this->m_nom_artiste = $p_nom_artiste;
        }
        public function getDate(){
            $annee = new datetime($this->m_date_album);
            return $annee->format("Y");
        }
        public function setDate($p_date_album){
            $this->m_date_album = $p_date_album;
        }
        public function getGenre(){
            return $this->m_genre;
        }
        public function setGenre($p_genre){
            $this->m_genre = $p_genre;
        }
        public function getOeuvres(){
            return $this->m_oeuvres;
        }
        public function setOeuvres($p_oeuvres){
            array_push($this->m_oeuvres, $p_oeuvres);
        }
        public function getPhtCouvt(){
            return $this->m_pht_couvt;
        }
        public function setPhtCouvt($p_pht_couvt){
            $this->m_pht_couvt = $p_pht_couvt;
        }
    }
?>
