<?php
class Artiste 
{   
    //Atributs
        private $m_id_artiste;
        private $m_nom;
        private $m_role;
        private $m_pays_origine;
        private $m_lien_photo;
    //Constructeur
        public function __construct($nom, $role, $paysOrigine, $lienPhoto){
            $this->m_nom = $nom;
            $this->m_role = $role;
            $this->m_pays_origine = $paysOrigine;
            $this->m_lien_photo = $lienPhoto;
        }
    // Getter/Setter
        public function getIdArtiste(){
            return $this->m_id_artiste;
        }
        public function setIDArtiste($p_id_artiste){
            $this->m_id_artiste = $p_id_artiste;
        }

        public function getNom(){
            return $this->m_nom;
        }
        public function setNom($p_nom){
            $this->m_nom = $p_nom;
        }

        public function getRole(){
            return $this->m_role;
        }
        public function setRole($p_role){
            $this->m_role = $p_role;
        }

        public function getPays(){
            return $this->m_pays_origine;
        }
        public function setPayse($p_pays_origine){
            $this->m_pays_origine = $p_pays_origine;
        }

        public function getLienPhoto(){
            return $this->m_lien_photo;
        }
        public function setLienPhoto($p_lien_photo){
            $this->m_lien_photo = $p_lien_photo;
        }





}



?>