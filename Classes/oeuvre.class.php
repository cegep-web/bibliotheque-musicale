<?php
    class Oeuvre
    {
    //Atributs
        private $m_id_album;
        private $m_titre;
        private $m_dureesec;
    //Constructeur
        public function __construct($id_album, $titre, $dureesec){
            $this->m_id_album = $id_album;
            $this->m_titre = $titre;
            $this->m_dureesec = $dureesec;
        }
    // Getter/Setter
        public function getIdAlbum(){
            return $this->m_id_album;
        }
        public function setIdAlbum($p_id_album){
            $this->m_id_album = $p_id_album;
        }
        public function getTitre(){
            return $this->m_titre;
        }
        public function setTitre($p_titre){
            $this->m_titre = $p_titre;
        }
        public function getDureeSec(){
            return $this->m_dureesec;
        }
        public function setDureeSec($p_dureesec){
            $this->m_titre = $p_dureesec;
        }
    //Méthodes
        public function getDureeMinutesSec(){
            return date('i:s', mktime(0, 0, $this->m_dureesec));
        }
    }
?>