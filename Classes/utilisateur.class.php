<?php
class Utilisateur
{   
    //Atributs
        private $m_id_utilisateur;
        private $m_nom;
        private $m_courriel;
        private $m_motDePasse;
        private $m_age;
    //Constructeur
        public function __construct($nom, $courriel, $motDePasse, $age){
            $this->m_nom = $nom;
            $this->m_courriel = $courriel;
            $this->m_motDePasse = $motDePasse;
            $this->m_age = $age;
        }

    // Getter/Setter
        public function getIdUtilisateur(){
            return $this->m_id_utilisateur;
        }
        public function setIdUtilisateur($p_id_utilisateur){
            $this->m_id_utilisateur = $p_id_utilisateur;
        }

        public function getNom(){
            return $this->m_nom;
        }
        public function setNom($p_nom){
            $this->m_nom = $p_nom;
        }

        public function getCourriel(){
            return $this->m_courriel;
        }
        public function setCourriel($p_courriel){
            $this->m_courriel = $p_courriel;
        }

        public function getMotDePasse(){
            return $this->m_motDePasse;
        }
        public function setMotdePasse($p_motDePasse){
            $this->m_motDePasse = $p_motDePasse;
        }

        public function getAge(){
            return $this->m_age;
        }
}



?>