<!-- Inclusion -->
    <?php
    require_once('../Includes/entete.php');

    //Determination de l'utilisateur 
    $CurrentUser = unserialize($_SESSION['utilisateur']);
    $CurrentUserNom = $CurrentUser->getNom();
    $CurrentUserId = $CurrentUser->getIdUtilisateur();
    ?>

<!-- Titre principale -->

<div id="corps">
    <div id="titre" class="container-md">
        <div class="row">
            <div class="col-12 mt-4 text-center text-primary">
                    <h2>Emprunter Une Oeuvre Ou la rendre</h2>
                </div>
            </div>
</div>

<!-- Contenu  -->
<div class="containter-fluid mainContainer">

    <div class="row">
        <div class="col-lg-2 col-sm-0"></div>
            <!-- Tableau oeuvre a rendre -->
            <div class="col-lg-8 col-sm-12 ContainerContenu" > 

                <h4>Oeuvre à rendre pour <?php echo($CurrentUserNom); ?></h4>
                    <?php
                        ListerOeuvreARendre($CurrentUserId);
                    ?>

                    <h4 id="rendreOeuvre">Rendre une oeuvre</h4>

                        <!-- FORM rendre une oeuvre -->
                <div class="col-lg-12 col-sm-12">
                    <form method="POST" enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" id="formulaireOr">
                        
                        <div class="form-row">
                            <div class="col-12">
                                <select name="listeRetour">
                                    <?php SelectOeuvreARendre($CurrentUserId) ?>
                                </select>
                            </div>
                            <div id="messageNomServer" class = "col-12"> </div>
                        </div>   

                        <button onclick="" type="submit" class="btn btn-primary col-12" id="btnSave" name="Retour" Value="IsRetourner">Rendre</button>

                        
                        <!--------- Execution d'un retour ---------->
                        <?php
                        if(isset($_POST['Retour'])){
                            if($_POST['Retour'] == "IsRetourner"){
                                
                                if(!empty($_POST['listeRetour'])){

                                    RendreOeuvre($CurrentUserId,$_POST['listeRetour']); 

                                    //Permet de rafraichir la page et de mettre a jour les tables d'emprunts ***
                                    echo "<meta http-equiv='refresh' content='0'>";        
                                }
                            
                            }
                        }
                        ?>

                    </form>
                </div>
                <div class="col-lg-3 col-sm-0"></div>
                 <!-------------------- Fin du form rendre un oeuvre *** ------------------->

            </div>
        <div class="col-lg-2 col-sm-0"></div>
    </div> 

    <div class="row">
        <div class="col-lg-2 col-sm-0"></div>
            <!-- Tableau oeuvre emprunte -->
            <div class="col-lg-8 col-sm-12 ContainerContenu" >
            <h4>Historique d'emprunt pour <?php  echo($CurrentUserNom) ?></h4>
                <?php
                    ListerEmpruntHistorique($CurrentUserId);
                ?>

                <h4 id="rendreOeuvre">Emprunter une oeuvre</h4>

                <!-- FORM emprunter une oeuvre  -->
                <div class="col-lg-12 col-sm-12" >
                <form method="POST" enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" id="formulaireEmprunt">   
                
                <div class="form-row">
                    <div class="col-12">
                        <select name="listeEmprunt">
                            <?php SelectOeuvreDisponible($CurrentUserId) ?>
                        </select>
                    </div>
                    <div id="messageNomServer" class = "col-12"> </div>
                </div>  
               
                <button onclick="" type="submit" class="btn btn-primary col-12" id="btnSave" name="Emprunter" Value="IsEmprunter">Emprunter</button>

                <!--------- Execution de l'emprunt ---------->
                <?php
                if(isset($_POST['Emprunter'])){
                    if($_POST['Emprunter'] == "IsEmprunter"){

                        if(!empty($_POST['listeEmprunt']) && isset($_POST['listeEmprunt'])){

                            EmprunterOeuvre($CurrentUserId,$_POST['listeEmprunt']);  
                            unset($_POST['Emprunter']); 

                            //Permet de rafraichir la page et de mettre a jour les tables d'emprunts ***
                            echo "<meta http-equiv='refresh' content='0'>";  
                        }
                    }
                }

                ?>

                </form>
                </div>
                <div class="col-lg-3 col-sm-0"></div>
                <!-------------------- Fin du form emprunter une oeuvre *** ------------------->

            </div>
        <div class="col-lg-2 col-sm-0"></div>
    </div>

</div>
<!---- fin du main Container -------------> 



<!-- //Inclusion du footer de page  -->
<?php require_once('../Includes/pied.php'); ?>
