<!DOCTYPE html>
<html>
<?php

    // Inclusions
    include('traitement.php');
    if (session_id() == ""){
        session_start();
    }

    // Vérification d'accès
    $page=$_SERVER['PHP_SELF'];

    if(strpos($page, "pageAlbum") == true || strpos($page, "pageArtiste") == true || strpos($page, "pageoeuvre") == true || strpos($page, "EmprunterOeuvre") == true){
        if(isset($_SESSION['utilisateur'])){
            $utilisateur = unserialize($_SESSION['utilisateur']);
            if (strpos($page, "EmprunterOeuvre") == false && ($utilisateur->getNom()) != "admin"){
                header("Location: ../SeConnecter/SeConnecter.php");
                exit();
            }
        }
        else
        {
            header("Location: ../SeConnecter/SeConnecter.php");
            exit();
        }
    }

    // Définition des variables
    if(strpos($page, "index") == true){
        $path = "";
        $titreDescription = "<title>Accueil</title><meta name='description' content='Accueil de la bibliothèque musicale'>";
        $accueil = "";
    }
    else
    {
        $path = "../";
        $accueil = "<li class='nav-item active'><a class='nav-link' href='../index.php'>Accueil <span class='sr-only'>(current)</span></a></li>";
        $optionsSousMenu = "<li><a class='dropdown-item' href='../AjouterUnAlbum/pageAlbum.php'>Un album</a></li><li><a class='dropdown-item' href='../AjouterOeuvre/pageoeuvre.php'>Une oeuvre</a></li><li><a class='dropdown-item' href='../AjouterArtiste/pageArtiste.php'>Un artiste</a></li>";
        $optionMenuListeAlbum = "<li><a class='dropdown-item' href='../ListeAlbums/pageListeAlbums.php'>Afficher les albums</a></li>";
        $optionMenuEmprunterOeuvre = "<li'><a class='dropdown-item' href='../EmprunterOeuvre/EmprunterOeuvre.php'>Emprunter une oeuvre</a></li>";
    }

    if(strpos($page, "pageAlbum") == true){
        $titreDescription = "<title>Ajouter un album</title><meta name='description' content='Permet d\'ajouter un album à la bibliothèque'>";
        $optionsSousMenu = "<li><a class='dropdown-item' href='../AjouterOeuvre/pageoeuvre.php'>Une oeuvre</a></li><li><a class='dropdown-item' href='../AjouterArtiste/pageArtiste.php'>Un artiste</a></li>";
    }
    else if(strpos($page, "pageArtiste") == true){
        $titreDescription = "<title>Ajouter un artiste</title><meta name='description' content='Permet d\'ajouter un artiste à la bibliothèque'>";
        $optionsSousMenu = "<li><a class='dropdown-item' href='../AjouterUnAlbum/pageAlbum.php'>Un album</a></li><li><a class='dropdown-item' href='../AjouterOeuvre/pageoeuvre.php'>Une oeuvre</a></li>";
    }
    else if(strpos($page, "AjouterOeuvre") == true){
        $titreDescription = "<title>Ajouter une oeuvre</title><meta name='description' content='Permet d\'ajouter une oeuvre à la bibliothèque'>";
        $optionsSousMenu = "<li><a class='dropdown-item' href='../AjouterUnAlbum/pageAlbum.php'>Un album</a></li><li><a class='dropdown-item' href='../AjouterArtiste/pageArtiste.php'>Un artiste</a></li>";
    }
    else if(strpos($page, "pageInscription") == true){
        if(isset($_SESSION['utilisateur'])){
            header("Location: ../EmprunterOeuvre/EmprunterOeuvre.php");
            exit();
        }
        $titreDescription = "<title>S'inscrire</title><meta name='description' content='Permet de s'inscrire en tant qu'utilisateur de la bibliothèque'>";
    }
    else if(strpos($page, "pageListeAlbums") == true){
        $titreDescription = "<title>Liste des albums</title><meta name='description' content='Permet de lister les albums de la bibliothèque'>";
        $optionMenuListeAlbum = "";
    }
    else if(strpos($page, "EmprunterOeuvre") == true){
        if(!isset($_SESSION['utilisateur'])){
            header("Location: ../SeConnecter/SeConnecter.php");
            exit();
        }
        $titreDescription = "<title>Emprunter ou restituer une oeuvre</title><meta name='description' content='Permet d'emprunter ou de restituer une oeuvre de la bibliothèque'>";
        $optionMenuEmprunterOeuvre = "";
    }
    else if(strpos($page, "SeConnecter") == true){
        if(isset($_SESSION['utilisateur'])){
            header("Location: ../EmprunterOeuvre/EmprunterOeuvre.php");
            exit();
        }
        $titreDescription = "<title>Se connecter</title><meta name='description' content='Permet de se connecter en tant qu'utilisateur de la bibliothèque'>";
    }

    if (isset($_SESSION['utilisateur'])){
        $connecte = $path."SeConnecter/SeDeconnecter.php'>Se Déconnecter";
    }
    else
    {
        $connecte = $path."SeConnecter/SeConnecter.php'>Se Connecter";
    }

    // Head
    echo "<head>";
    echo "<meta charset='utf-8'>";
    echo $titreDescription;
?>

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<?php
    // Feuilles de style
    echo "<link rel='stylesheet' href='".$path."basestyles.css'>";
?>
        <link rel="stylesheet" href="Stylesheet.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    </head><!--------------------------------Fin du Head------------------------------------------------------->
    <body>

        <nav class="navbar navbar-expand-sm navbar-light row sticky-top" id="entete" style="background-color: #e3f2fd;">
            <div class="col-12 col-sm-1">
<?php
    echo "<a class='navbar-brand mb-0 h1 text-primary'><img src='".$path."assets/Logo.png' width='60' height='60' alt='MM Logo'></a>";
?>
            </div>
            <div class="col-12 col-sm-7 col-md-8 col-lg-9 d-sm-inline-flex align-items-center ml-1 ml-sm-2 ml-md-0">
                <ul class="navbar-nav">
<?php
    echo $accueil;
    echo "<li class='nav-item active'>";
    echo "<a class='nav-link text-primary' id='Sign' href='".$connecte." <span class='sr-only'>(current)</span></a>";
    echo "</li>";
    echo "</ul>";
    echo "</div>";
    if(strpos($page, "index") == false){

        /* Le code suivant comporte une partie qui a permis à la création d'un sous-menu de la nav Bootstrap
            Le code source a été modifié au besoin pour l'adapter au contexte et du résultat voulu.
            Solution trouvée sur le site StackOverFlow à l'adresse suivante:
            Source: "https://stackoverflow.com/questions/44467377/bootstrap-4-multilevel-dropdown-inside-navigation" */

        echo "<div class='col-12 col-sm-5 col-md-4 col-lg-3 pl-1'>";
        echo "<div class='ml-3 ml-sm-0'>";
        echo "<ul class='navbar-nav ml-auto'>";
        echo "<li class='nav-item dropdown'>";
        echo "<a class='nav-link dropdown-toggle text-primary' href='#' id='navbarDropdownMenuLink' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Menu </a>";
        echo "<ul class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>";
        echo $optionMenuListeAlbum;
        echo $optionMenuEmprunterOeuvre;
        echo "<li class='dropdown-submenu'>";
        echo "<a href='#' class='dropdown-toggle dropdown-item' data-toggle='dropdown' id='navbarDropdownSubMenuLink'>Ajouter </a>";
        echo "<ul class='dropdown-menu'>";
        echo $optionsSousMenu;
        echo "</ul>";
        echo "</li>";
        echo "</ul>";
        echo "</div>";
        echo "</div>";

        /* Fin de citation */

    }
?>
        </nav><!-------------------------Fin de la Nav----------------------------------------------------->
