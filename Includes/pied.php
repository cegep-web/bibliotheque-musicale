        <footer id="pied">
            <h5>Merci d'utiliser notre Bibliothèque Musicale!</h5>
        </footer>

    <!-- jQuery, Popper.js, Bootstrap JS -->

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<?php

    $nomDePage = explode("/", $page);
    $document = end($nomDePage);
    $extraction_extension = explode(".", $document);
    $nomDocument = $extraction_extension[0];

    if($nomDocument == "pageArtiste" || $nomDocument == "pageoeuvre" || $nomDocument == "pageAlbum" || $nomDocument == "pageIncription")
    {
        echo "<script type='text/javascript' src='".$nomDocument.".js'></script>";
    }
?>

    <script>
    $(document).ready(function(){
        ArrangerHauteur();
        $( window ).resize(function() {
            ArrangerHauteur();
        });

    });

    function ArrangerHauteur(){
        $e = $("#entete").height();
        $c = $("#corps").height();
        $t = $("#titre").height();
        $p = $("#pied").height();
        $w = $(window).height();

        $c = $w - $e - $t - $p;
        if($c < 525){
            $c=525;
        }

        $("#corps").height($c);
    }

    /* Code d'une fonction permettant de faire fonctionner le sous-menu de la nav Bootstrap
        Solution trouvée sur le site StackOverFlow à l'adresse suivante:
        Source: "https://stackoverflow.com/questions/44467377/bootstrap-4-multilevel-dropdown-inside-navigation" */

    $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
        if (!$(this).next().hasClass('show')) {
            $(this).parents('.dropdown-menu').first().find('.show').removeClass('show');
            $('#navbarDropdownSubMenuLink').removeClass('active');
        }
        var $subMenu = $(this).next('.dropdown-menu');
        $subMenu.toggleClass('show');
        $('#navbarDropdownSubMenuLink').toggleClass('active');

        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
            $('.dropdown-submenu .show').removeClass('show');
            $('#navbarDropdownSubMenuLink').removeClass('active');
        });

        return false;
    });

    /* Fin de citation */

    </script>

<?php
    if(isset($_SESSION['formSoumise']) && $_SESSION['formSoumise'] === true){
        echo "<script>$('#modal').modal('show')</script>";
        unset($_SESSION['formSoumise']);
    }
?>

    </body><!--------------------------------Fin du body------------------------------------------------------->
</html>

