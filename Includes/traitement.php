<?php

// Intégration des classes:

$page=$_SERVER['PHP_SELF'];

if(strpos($page, "index") == false){
	require('../Classes/album.class.php');
	require('../Classes/utilisateur.class.php');
	require('../Classes/artiste.class.php');
}

// Fonctions de bases:

function ValiderEntree($data)
{
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

// Connection à la base de données:

function SeConnecter(){
	try
	{
		$servername = 'localhost';
		$username = 'root';
		$password = '';
		$bd='media_db';
		$conn = new PDO("mysql:host=$servername;dbname=$bd;charset=utf8",$username, $password);
	}
	catch (Exception $e)
	{
		die('Erreur lors de la connexion à la BD : ' . $e->getMessage());
	}
	return $conn;
}

//-------------------------------PAGE SE CONNECTER----------------------------------------------------

function ConnecterUtilisateur($p_courriel,$p_passe){
	$reqsql="SELECT * FROM utilisateur ".
	" where courriel=:pcourriel";
	$conn=SeConnecter();
	$reponse=$conn->prepare($reqsql);
	$reponse->bindParam(':pcourriel', $p_courriel, PDO::PARAM_STR);
	$reponse->execute();
	$id=-1;
	while ($utilisateurs = $reponse->fetch())
	{
		$passe_bd = $utilisateurs['mot_passe'];
		if(password_verify($p_passe, $passe_bd))
		{
			$id= $utilisateurs['id_utilisateur'];
			$nom= $utilisateurs['nom'];
			$courriel = $utilisateurs['courriel'];
			$age= $utilisateurs['age'];
			$utilisateur=new Utilisateur($nom, $courriel, $p_passe, $age); 
			$utilisateur->setIdUtilisateur($id);
		}
	}
	$reponse->closeCursor();
	if ($id == -1){
		return null;
	}
	return $utilisateur;
}

//---------------------------------PAGE AJOUTER ALBUM-------------------------------------------------

function ListerGenres($p_id_genre)
{
	try
	{
		$reqsql = "SELECT * FROM genre;";
		$conn = SeConnecter();
		$reponse = $conn -> prepare($reqsql);
		$reponse -> execute();
		$count = $reponse -> rowCount();
		echo "<option value='-1'>Choisir un genre</option>";
		while($genres = $reponse->fetch()){
			echo "<option value'".$genres['id_genre']."'";
			if($p_id_genre == $genres['id_genre'])
			{
				echo " selected='selected'";
			}
			echo ">".$genres['description']."</option>";
		}
		$reponse->closeCursor();
		$conn = null;
	}
	catch(PDOException $e)
	{
		echo "Erreur : ".$e -> getMessage();
	}
}

function TrouverIdGenre($p_genre)
{
	try
	{
		$reqsql = "SELECT * FROM genre WHERE `description`=?;";
		$conn = SeConnecter();
		$reponse = $conn -> prepare($reqsql);
		$reponse -> bindParam(1, $p_genre);
		$reponse -> execute();
		$conn = null;
		$genre = $reponse -> fetch();
		$id_genre = $genre['id_genre'];
		return $id_genre;
	}
	catch(PDOException $e)
	{
		echo "Erreur : ".$e -> getMessage();
		return false;
	}
}

function VerifierAlbum($p_nom)
{
	try
	{
		$reqsql = "SELECT * FROM album WHERE nom=?;";
		$conn = SeConnecter();
		$reponse = $conn -> prepare($reqsql);
		$reponse -> bindParam(1, $p_nom);
		$reponse -> execute();
		$count = $reponse -> rowCount();
		$conn = null;
		if($count > 0){
			return true;
		}
		else
		{
			return false;
		}
	}
	catch(PDOException $e)
	{
		echo "Erreur : ".$e -> getMessage();
		return false;
	}
}

function AjouterAlbum($p_nom, $p_date, $p_id_genre, $p_pht_couvt){
	
	try
	{
		$reqsql = "INSERT INTO album (nom, date_album, id_genre, pht_couvt) VALUES (?,?,?,?);";
		$conn = SeConnecter();
		$reponse = $conn -> prepare($reqsql);
		$reponse -> execute(array($p_nom, $p_date, $p_id_genre, $p_pht_couvt));
		$conn = null;
		return true;
	}
	catch(PDOException $e)
	{
		echo "Erreur : ".$e -> getMessage();
		return false;
	}
}

function TelechargerCouverture(){
	$nom_fichier = $_FILES['couverture']['name'];
	$fichier_temporaire = $_FILES['couverture']['tmp_name'];
	$extension_acceptees = array("jpg", "JPG", "gif", "GIF", "png", "PNG", "jpeg", "JPEG", "bmp", "BMP", "svg", "SVG");
	$extraction_extension = explode(".", $nom_fichier);
	$extension = end($extraction_extension);
	$destination = "../Couvertures/".$nom_fichier;
	if(in_array($extension, $extension_acceptees)){
		try
		{
			move_uploaded_file($fichier_temporaire, $destination);
			return true;
		}
		catch(PDOException $e){
			echo $e->getMessage();
			return false;
		}
	}
	else
	{
		return false;
	}
}

//---------------------------------------PAGE LISTE D'ALBUMS------------------------------------------------

function AfficherAlbums(){
	try
	{
		$reqsql = "SELECT a.id_album, a.nom, i.nom_artiste, a.date_album, g.description, o.titre_oeuvre, o.dureesec, a.pht_couvt FROM album AS a LEFT JOIN genre AS g ON a.id_genre = g.id_genre LEFT JOIN oeuvre AS o ON a.id_album = o.id_album LEFT JOIN artiste AS i ON o.id_artiste = i.id_artiste ORDER BY a.nom;";
		$conn = SeConnecter();
		$reponse = $conn -> prepare($reqsql);
		$reponse->execute();
		$id = -1;
		$albums = array();
		while($row = $reponse->fetch())
		{
			$id_album = $row['id_album'];
			$nom = $row['nom'];
			$nom_artiste = $row['nom_artiste'];
			$date_album = $row['date_album'];
			$genre = $row['description'];
			$titre_oeuvre = $row['titre_oeuvre'];
			$dureesec = $row['dureesec'];
			$pht_couvt = $row['pht_couvt'];
			if(count($albums) == 0 || !array_key_exists($id_album, $albums))
			{
				$nouvelAlbum = new Album($id_album, $nom, $date_album, $genre, $pht_couvt);
				if($nom_artiste != "")
				{
					$nouvelAlbum->setNomArtiste($nom_artiste);
				}
				$oeuvre = new Oeuvre($id_album, $titre_oeuvre, $dureesec);
				$nouvelAlbum->setOeuvres($oeuvre);
				$albums[$id_album] = $nouvelAlbum;
			}
			else
			{
				$oeuvre = new Oeuvre($id_album, $titre_oeuvre, $dureesec);
				$albums[$id_album]->setOeuvres($oeuvre);
			}
		}
		$conn = null;
		return $albums;
	}
	catch(PDOException $e){
		echo $e->getMessage();
		return false;
	}
}

//--------------------------------------PAGE AJOUTER ARTISTE--------------------------------

//Verifier si un artiste existe dans la BD et retourne un bool 
function VerifierArtiste($p_nom){
    try
        {
            $reqsql = "SELECT * FROM artiste WHERE nom_artiste=?;";
            $conn = SeConnecter();
            $reponse = $conn -> prepare($reqsql);
            $reponse -> bindParam(1, $p_nom);
            $reponse -> execute();
            $count = $reponse -> rowCount();
            $conn = null;
            
            if($count > 0){
                return true;
            }
            else
            {
                return false;
            }
        }
        catch(PDOException $e)
        {
            echo "Erreur : ".$e -> getMessage();
            return false;
        }
}

// Permet d'ajouter un artiste a la BD
function AjouterArtiste($p_nom, $p_role, $p_paysOrigine, $p_lienPhoto){
try
    {
        $reqsql = "INSERT INTO artiste (nom_artiste, `role`, pht_artiste , pays_artiste) VALUES (?,?,?,?);";
        $conn = SeConnecter();
        $reponse = $conn -> prepare($reqsql);
        $reponse -> execute(array($p_nom, $p_role, $p_lienPhoto, $p_paysOrigine));
        $conn = null;
        return true;
    }
    catch(PDOException $e)
    {
        echo "Erreur : ".$e -> getMessage();
        return false;
    }
}



//Permet d'obtenir la liste d'artiste fesant partie de la BD
function ListerArtiste(){
    SeConnecter();


}

//-------------------------------------------------PAGE AJOUTER OEUVRE------------------------------------

function ListerChanteur($p_id_chanteur)
{
	try
	{
		$reqsql = "SELECT * FROM artiste;";
		$conn = SeConnecter();
		$reponse = $conn -> prepare($reqsql);
		$reponse -> execute();
		$count = $reponse -> rowCount();
		echo "<option value='-1'>Choisir un artiste</option>";
		while($artiste = $reponse->fetch()){
			echo "<option value'".$artiste['id_artiste']."'";
			if($p_id_chanteur == $artiste['id_artiste'])
			{
				echo " selected='selected'";
			}
			echo ">".$artiste['nom_artiste']."</option>";
		}
		$reponse->closeCursor();
		$conn = null;
	}
	catch(PDOException $e)
	{
		echo "Erreur : ".$e -> getMessage();
	}
}

function ListerAlbum($p_id_album)
{
	try
	{
		$reqsql = "SELECT * FROM album;";
		$conn = SeConnecter();
		$reponse = $conn -> prepare($reqsql);
		$reponse -> execute();
		$count = $reponse -> rowCount();
		echo "<option value='-1'>Choisir un album</option>";
		while($album = $reponse->fetch()){
			echo "<option value'".$album['id_album']."'";
			if($p_id_album == $album['id_album'])
			{
				echo " selected='selected'";
			}
			echo ">".$album['nom']."</option>";
		}
		$reponse->closeCursor();
		$conn = null;
	}
	catch(PDOException $e)
	{
		echo "Erreur : ".$e -> getMessage();
	}
}

function AjouterOeuvre($p_titre, $p_id_artiste, $p_duree_seconde, $p_taille_mb, $p_lyrics, $p_date_ajout, $p_id_album){
	try
		{
			$reqsql = "INSERT INTO oeuvre (titre_oeuvre, id_artiste, dureesec, taillemb, lyrics, date_ajout, id_album) VALUES (?,?,?,?,?,?,?);";
			$conn = SeConnecter();
			$reponse = $conn -> prepare($reqsql);
			$reponse -> execute(array($p_titre, $p_id_artiste, $p_duree_seconde, $p_taille_mb, $p_lyrics,$p_date_ajout, $p_id_album ));
			$conn = null;
			return true;
		}
		catch(PDOException $e)
		{
			echo "Erreur : ".$e -> getMessage();
			return false;
		}
	}



function TrouverIdOeuvre($p_oeuvre)
{
	try
	{
		$reqsql = "SELECT * FROM oeuvre WHERE `titre_oeuvre`=?;";
		$conn = SeConnecter();
		$reponse = $conn -> prepare($reqsql);
		$reponse -> bindParam(1, $p_oeuvre);
		$reponse -> execute();
		$conn = null;
		$oeuvre = $reponse -> fetch();
		$id_oeuvre = $oeuvre['id_oeuvre'];
		return $id_oeuvre;
	}
	catch(PDOException $e)
	{
		echo "Erreur : ".$e -> getMessage();
		return false;
	}
}

function TrouverIdArtiste($p_artiste)
{
	try
	{
		$reqsql = "SELECT * FROM artiste WHERE `nom_artiste`=?;";
		$conn = SeConnecter();
		$reponse = $conn -> prepare($reqsql);
		$reponse -> bindParam(1, $p_artiste);
		$reponse -> execute();
		$conn = null;
		$artiste = $reponse -> fetch();
		$id_artiste = $artiste['id_artiste'];
		return $id_artiste;
	}
	catch(PDOException $e)
	{
		echo "Erreur : ".$e -> getMessage();
		return false;
	}
}
	
function TrouverIdAlbum($p_album)
{
	try
	{
		$reqsql = "SELECT * FROM album WHERE `nom`=?;";
		$conn = SeConnecter();
		$reponse = $conn -> prepare($reqsql);
		$reponse -> bindParam(1, $p_album);
		$reponse -> execute();
		$conn = null;
		$album = $reponse -> fetch();
		$id_album = $album['id_album'];
		return $id_album;
	}
	catch(PDOException $e)
	{
		echo "Erreur : ".$e -> getMessage();
		return false;
	}
}

//-------------------------------------------------PAGE AJOUTER UTILISATEUR-------------------------------

//Verifier si un nom d'utilisateur existe dans la BD et retourne un bool 
function VerifierUtilisateur($p_nom){
    try
        {
            $reqsql = "SELECT * FROM utilisateur WHERE nom=?;";
            $conn = SeConnecter();
            $reponse = $conn -> prepare($reqsql);
            $reponse -> bindParam(1, $p_nom);
            $reponse -> execute();
            $count = $reponse -> rowCount();
            $conn = null;
            
            if($count > 0){
                return true;
            }
            else
            {
                return false;
            }
        }
        catch(PDOException $e)
        {
            echo "Erreur : ".$e -> getMessage();
            return false;
        }
}
//Verifier si un courriel existe dans la BD et retourne un bool 
function VerifierCourriel($p_courriel){
    try
        {
            $reqsql = "SELECT * FROM utilisateur WHERE courriel=?;";
            $conn = SeConnecter();
            $reponse = $conn -> prepare($reqsql);
            $reponse -> bindParam(1, $p_courriel);
            $reponse -> execute();
            $count = $reponse -> rowCount();
            $conn = null;
            
            if($count > 0){
                return true;
            }
            else
            {
                return false;
            }
        }
        catch(PDOException $e)
        {
            echo "Erreur : ".$e -> getMessage();
            return false;
        }
}

// Permet d'ajouter un utilisateur a la BD
function AjouterUtilisateur($p_nom, $p_courriel, $p_motDePasse, $p_age){
try
    {
		$p_motDePasse = password_hash($p_motDePasse, PASSWORD_DEFAULT);

        $reqsql = "INSERT INTO utilisateur (nom, courriel, mot_passe , age) VALUES (?,?,?,?);";
        $conn = SeConnecter();
        $reponse = $conn -> prepare($reqsql);
        $reponse -> execute(array($p_nom, $p_courriel, $p_motDePasse, $p_age));
        $conn = null;
        return true;
    }
    catch(PDOException $e)
    {
        echo "Erreur : ".$e -> getMessage();
        return false;
    }
}

//---------------------------------------PAGE EMPRUNTER OEUVRE-------------------------------

function ListerEmpruntHistorique($p_id_utilisateur){
	try
	{
		$reqsql = "SELECT titre_oeuvre AS leTitre,date_emprunt,date_retour FROM emprunts 
		           INNER JOIN oeuvre ON emprunts.id_oeuvre = oeuvre.id_oeuvre 
				   WHERE id_utilisateur = $p_id_utilisateur && date_retour IS NOT NULL
				   ORDER BY date_retour;";
		$conn = SeConnecter();
		$reponse = $conn -> prepare($reqsql);
		$reponse -> execute();
		$count = $reponse -> rowCount();

		echo("<table class='tableStyle'> ");
		echo("<tr>");
		echo("<th> Titre </th>");
		echo("<th> Date d'emprunt </th>");
		echo("<th> Date de retour </th>");
		echo("</tr>");

		while($emprunt = $reponse->fetch()){
			echo("<tr>");
			echo("<td>". $emprunt['leTitre']. "</td>");
			echo("<td>".$emprunt['date_emprunt']."</td>");
			echo("<td>".$emprunt['date_retour']."</td>");
			echo("</tr>");
		}

		echo "</table>";

		$reponse->closeCursor();
		$conn = null;
	}
	catch(PDOException $e)
	{
		echo "Erreur : ".$e -> getMessage();
	}
}

function ListerOeuvreARendre($p_id_utilisateur){
	if (checkIfOeuvreARendre($p_id_utilisateur) == true){
		try
		{
			$reqsql = "SELECT titre_oeuvre AS leTitre,date_emprunt,date_retour FROM emprunts 
			           INNER JOIN oeuvre ON emprunts.id_oeuvre = oeuvre.id_oeuvre 
					   WHERE id_utilisateur = $p_id_utilisateur && date_retour IS NULL
					   ORDER BY date_emprunt;";
			$conn = SeConnecter();
			$reponse = $conn -> prepare($reqsql);
			$reponse -> execute();
			$count = $reponse -> rowCount();

			echo("<table class='tableStyle'> ");
			echo("<tr>");
			echo("<th> Titre </th>");
			echo("<th> Date d'emprunt </th>");
			echo("<th> Date de retour </th>");
			echo("</tr>");

			while($emprunt = $reponse->fetch()){
				echo("<tr>");
				echo("<td>".$emprunt['leTitre']. "</td>");
				echo("<td>".$emprunt['date_emprunt']."</td>");
				echo("<td>".$emprunt['date_retour']."En attente... </td>");
				echo("</tr>");
			}

			echo "</table>";

			$reponse->closeCursor();
			$conn = null;
		}
		catch(PDOException $e)
		{
			echo "Erreur : ".$e -> getMessage();
		}
	}
	else if (checkIfOeuvreARendre($p_id_utilisateur) == false){
		echo("<table class='tableStyle'> ");
			echo("<tr>");
			echo("<th> Titre </th>");
			echo("<th> Date d'emprunt </th>");
			echo("<th> Date de retour </th>");
			echo("</tr>");
			echo("<tr>");
				echo("<td>Aucune oeuvre à rendre pour le moment!</td>");
				echo("<td> </td>");
				echo("<td> </td>");
			echo("</tr>");
		echo "</table>";
	}
}


//Lister tous les oeuvre a rendre dans un select
function SelectOeuvreArendre($p_id_utilisateur){
	try
	{
		$reqsql = "SELECT titre_oeuvre AS leTitre,date_emprunt,date_retour,emprunts.id_oeuvre FROM emprunts
		INNER JOIN oeuvre ON emprunts.id_oeuvre = oeuvre.id_oeuvre 
		WHERE id_utilisateur = $p_id_utilisateur && date_retour IS NULL ORDER BY titre_oeuvre;";
		$conn = SeConnecter();
		$reponse = $conn -> prepare($reqsql);
		$reponse -> execute();

		echo("<option value='-1'>Choisir une oeuvre</option>");
		while($oeuvreArendre = $reponse->fetch()){
			echo("<option value = ".$oeuvreArendre['id_oeuvre']. "> ".$oeuvreArendre['leTitre']." </option>");
		}
		$reponse->closeCursor();
		$conn = null;
	}
	catch(PDOException $e)
	{
		echo "Erreur : ".$e -> getMessage();
	}
}

function SelectOeuvreDisponible($p_id_utilisateur){
	if (checkIfOeuvreARendre($p_id_utilisateur) == true){
		try
		{
			$reqsql = "SELECT id_oeuvre,titre_oeuvre FROM oeuvre WHERE id_oeuvre NOT IN
			(SELECT id_oeuvre FROM emprunts  WHERE id_utilisateur = $p_id_utilisateur && date_retour IS NULL) 
			ORDER BY titre_oeuvre;";
			$conn = SeConnecter();
			$reponse = $conn -> prepare($reqsql);
			$reponse -> execute();

			echo("<option value='-1'>Choisir une oeuvre</option>");
			while($oeuvreArendre = $reponse->fetch()){
				echo("<option value = ".$oeuvreArendre['id_oeuvre']. "> ".$oeuvreArendre['titre_oeuvre']." </option>");
			}
			$reponse->closeCursor();
			$conn = null;
		}
		catch(PDOException $e)
		{
			echo "Erreur : ".$e -> getMessage();
		}
	}
	else if (checkIfOeuvreARendre($p_id_utilisateur) == false){
		try
		{
			$reqsql = "SELECT titre_oeuvre, id_oeuvre FROM oeuvre";
			$conn = SeConnecter();
			$reponse = $conn -> prepare($reqsql);
			$reponse -> execute();

			echo("<option value='-1'>Choisir une oeuvre</option>");
			while($oeuvreArendre = $reponse->fetch()){
				echo("<option value = ".$oeuvreArendre['id_oeuvre']. "> ".$oeuvreArendre['titre_oeuvre']." </option>");
			}
			$reponse->closeCursor();
			$conn = null;
		}
		catch(PDOException $e)
		{
			echo "Erreur : ".$e -> getMessage();
		}
	}
}

// Regarde si un utilisateur a des oeuvres a rendre et retourne un bool
function checkIfOeuvreARendre($p_id_utilisateur){
	try
	{
		$reqsql = "SELECT titre_oeuvre AS leTitre,date_emprunt,date_retour FROM emprunts
		INNER JOIN oeuvre ON emprunts.id_oeuvre = oeuvre.id_oeuvre 
		WHERE id_utilisateur = $p_id_utilisateur && date_retour IS NULL;";
		$conn = SeConnecter();
		$reponse = $conn -> prepare($reqsql);
		$reponse -> execute();
		$count = $reponse->rowCount();

		if($count < 1){
			return false;
		}
		else{
			return true;
		}

		$conn = null;
	}
	catch(PDOException $e)
	{
		echo "Erreur : ".$e -> getMessage();
	}
}

function EmprunterOeuvre($p_id_utilisateur,$p_id_oeuvre){
	try
    {
        $reqsql = "INSERT INTO emprunts (id_utilisateur,id_oeuvre) VALUES (?,?);";
        $conn = SeConnecter();
        $reponse = $conn -> prepare($reqsql);
        $reponse -> execute(array($p_id_utilisateur, $p_id_oeuvre));
        $conn = null;
		return true;
    }
    catch(PDOException $e)
    {
        echo "Erreur : ".$e -> getMessage();
        return false;
    }
}

function RendreOeuvre($p_id_utilisateur,$p_id_oeuvre){
	try
    {
        $reqsql = "UPDATE emprunts SET date_retour = CURRENT_TIMESTAMP WHERE id_utilisateur = ? && id_oeuvre = ?;";
        $conn = SeConnecter();
        $reponse = $conn -> prepare($reqsql);
        $reponse -> execute(array($p_id_utilisateur, $p_id_oeuvre));
        $conn = null;
		return true;
    }
    catch(PDOException $e)
    {
        echo "Erreur : ".$e -> getMessage();
        return false;
    }
}
?>