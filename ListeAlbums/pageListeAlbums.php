<?php
    require_once('../Includes/entete.php');
?>
    <div id="corps">
        <div id="titre" class="container-md">
            <div class="row">
                <div class="col-12 mt-4 text-center text-primary">
                    <h1>Liste des albums</h1>
                </div>
            </div>
        </div>
        <div class="container-fluid" id="backgroundedContainer">
            <div class="row" id="list">
                <?php
                    $albums = AfficherAlbums();
                    foreach($albums as $album)
                    {
                        echo "<div class='alert alert-primary border-primary col-lg-3 col-md-4 col-sm-6 pr-0'>";
                            echo "<div class='row'>";
                                echo "<div class='col-sm-5 col-xl-4 mb-2 mr-2'>";
                                    echo "<div class='row pl-2 justify-content-center justify-content-sm-start'>";
                                        echo "<img class='border border-primary rounded' src='/Couvertures/".$album->getPhtCouvt()."' alt='Album Cover' width='100' height='100'>";
                                    echo "</div>";
                                echo "</div>";
                                echo "<div class='col-sm-6'>";
                                    echo "<div class='row justify-content-center justify-content-sm-start'>";
                                        echo "<h5 class='text-dark font-weight-bold mb-1'>".$album->getNom()."</h5>";
                                    echo "</div>";
                                    echo "<div class='row justify-content-center justify-content-sm-start'>";
                                        echo "<p class='text-primary mb-1'>".$album->getNomArtiste()."</h4>";
                                    echo "</div>";
                                    echo "<div class='row justify-content-center justify-content-sm-start'>";
                                        echo "<p class='text-secondary mb-1'>".$album->getDate()."</p>";
                                    echo "</div>";
                                    echo "<div class='row justify-content-center justify-content-sm-start'>";
                                        echo "<p class='text-secondary mb-1'>".$album->getGenre()."</p>";
                                    echo "</div>";
                                echo "</div>";
                                echo "<div class='row pl-3 p-2 w-100 justify-content-center'>";
                                $oeuvres = $album->getOeuvres();
                                if($oeuvres[0]->getTitre() == "")
                                {
                                    echo "<div class='col-10 mt-4'>";
                                    echo "<p class='text-center'><small class='text-secondary'>Aucune oeuvre enregistrée à ce jour pour cet album, vous pouvez en ajouter <a href='../AjouterOeuvre/pageoeuvre.php'>ici</a> si vous le désirez.</small></p>";
                                    echo "</div>";
                                }
                                else
                                {
                                    echo "<table class='table table-striped'>";
                                        echo "<thead>";
                                            echo "<tr class='table-primary'>";
                                                echo "<th scope='col'>Titre:</th>";
                                                echo "<th scope='col'>Durée:</th>";
                                            echo "</tr>";
                                        echo "</thead>";
                                        echo "<tbody>";
                                        foreach($oeuvres as $oeuvre)
                                        {
                                            echo "<tr>";
                                                echo "<td><small>".$oeuvre->getTitre()."</small></td>";
                                                echo "<td class='text-right'>".$oeuvre->getDureeMinutesSec()."</td>";
                                            echo "</tr>";
                                        }
                                        echo "</tbody>";
                                    echo "</table>";
                                }
                                echo "</div>";
                            echo "</div>";
                        echo "</div>";
                    
                    }
                ?>
            </div>
        </div>
    </div>
<?php
    require_once('../Includes/pied.php');
?>