<?php
    require_once('../Includes/entete.php');

    $valide = "yes";

    if (isset($_POST["connecter"])) {
        $courriel = htmlspecialchars($_POST["courriel"]);
        if (isset($_POST["passe"])) {
            $passe = htmlspecialchars($_POST["passe"]);
            $utilisateur=ConnecterUtilisateur($courriel,$passe);
            if (!is_null($utilisateur)){
                $_SESSION['utilisateur'] = serialize($utilisateur);
                $valide="yes";
                header("Location: ../EmprunterOeuvre/EmprunterOeuvre.php");
                exit();
            }
            else
            {
                $valide='no';
            }
        }
    }
?>
        <div id="corps">
            <div id="titre" class="container-md">
                <div class="row">
                    <div class="col-12 mt-4 text-center text-primary">
                        <h1>Se Connecter</h1>
                    </div>
                </div>
            </div>
            <div class="mainContainer container" id="backgroundedContainer">
                <div class="row justify-content-center">
                    <div class="col-lg-6 col-md-8" id="formContainer"> 
                        <form class="mt-4" method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                            <div class="row form-group">
                                <label class="col-md-5 col-form-label text-secondary" for="courriel">Courriel:</label>
                                <div class="col-md-7">
                                    <input type="text" id="courriel" name="courriel" class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-5 col-form-label text-secondary" for="motDePasse">Mot de passe:</label>
                                <div class="col-md-7">
                                    <input type="password" id="motDePasse" name="passe" class="form-control">
                                </div>
                            </div>
<?php
    if($valide == "no"){
        echo "<p class='text-danger text-center'>L'identifiant ou le mot de passe est incorrect.</p>";
        echo "<button type='submit' class='btn btn-primary' id='btnSave' name='connecter' value='Connecter'>Se Connecter</button>";
    }
    else
    {
        echo "<button type='submit' class='btn btn-primary mt-5 mb-5' id='btnSave' name='connecter' value='Connecter'>Se Connecter</button>";
    }
?>
                        </form><!------------------------ Fin du form *** ----------------------->
                        <hr class="mb-4">
                        <p class="text-dark text-center">Vous voulez vous inscrire? Vous pouvez le faire <a href="/AjouterUtilisateur/AjouterUtilisateur.php">ici</a>.</p>
                    </div>
                </div>             
            </div>
        </div>
<?php
    require_once('../Includes/pied.php');
?>
