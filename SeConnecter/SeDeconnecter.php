<?php
    if (session_id() == ""){
        session_start();
    }
    session_destroy();
    session_unset();
    $page=$_SERVER['PHP_SELF'];
    if(strpos($page, "index") == false){
        header("Location: ../index.php");
    }
    else
    {
        header("Location: index.php");
    }
?>