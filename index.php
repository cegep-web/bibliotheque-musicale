<?php
	require_once("Includes/entete.php");
?>
        <div id="corps">
            <div id="titre" class="container-md">
                <div class="row">
                    <div class="col-12 mt-4 text-center text-primary">
						<h1>Bienvenue sur la Médiathèque Musicale de Programmation Web II</h1>
						<h6>Voici les options que vous pouvez sélectionner.</h6>
					</div>
                </div>
            </div>
			<div class="container-fluid mainContainer">
				<hr>  
				<div class="row justify-content-center">
					<div class="col-md-5 col-xl-3 m-3 backgrounded" id="utilisateur">
						<a class="links" href="AjouterUtilisateur/pageInscription.php">
							<span class="clickable d-flex align-items-center justify-content-center">
								<p class="font-weight-bold m-0 text-center">S'inscrire</p>
							</span>
						</a>
					</div>
					<div class="col-md-5 col-xl-3 m-3 backgrounded" id="listeAlbums">
						<a class="links" href="ListeAlbums/pageListeAlbums.php">
							<span class="clickable d-flex align-items-center justify-content-center">
								<p class="font-weight-bold m-0 text-center">Voir les albums</p>
							</span>
						</a>
					</div>
					<div class="col-md-5 col-xl-3 m-3 backgrounded" id="emprunt">
						<a class="links" href="EmprunterOeuvre/EmprunterOeuvre.php">
							<span class="clickable d-flex align-items-center justify-content-center">
								<p class="font-weight-bold m-0 p-2 text-center">Emprunter ou restituer une oeuvre</p>
							</span>
						</a>
					</div>
					<div class="col-md-5 col-xl-3 m-3 backgrounded" id="oeuvre">
						<a class="links" href="AjouterOeuvre/pageoeuvre.php">
							<span class="clickable d-flex align-items-center justify-content-center">
								<p class="font-weight-bold m-0 text-center">Ajouter une oeuvre</p>
							</span>
						</a>
					</div>
					<div class="col-md-5 col-xl-3 m-3 backgrounded" id="artiste">
						<a class="links" href="AjouterArtiste/pageArtiste.php">
							<span class="clickable d-flex align-items-center justify-content-center">
								<p class="font-weight-bold m-0 text-center">Ajouter un artiste</p>
							</span>
						</a>   
					</div>
					<div class="col-md-5 col-xl-3 m-3 backgrounded" id="album">
						<a class="links" href="AjouterUnAlbum/pageAlbum.php">
							<span class="clickable d-flex align-items-center justify-content-center">
								<p class="font-weight-bold m-0 text-center">Ajouter un album</p>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>
<?php
	require_once("Includes/pied.php");
?>