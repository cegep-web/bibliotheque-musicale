DROP DATABASE IF EXISTS media_db;
CREATE DATABASE media_db;
USE media_db;

--
-- Database: `media_db`
--


--
-- Table structure for table `genre`
--

CREATE TABLE IF NOT EXISTS `genre` (
  `id_genre` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) NOT NULL,
  PRIMARY KEY (`id_genre`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`id_genre`, `description`) VALUES
(1, 'POP'),
(2, 'Country'),
(3, 'Flamenco'),
(4, 'Hard rock'),
(5, 'Disco'),
(6, 'Rock'),
(7, 'Métal'),
(8, 'Jazz'),
(9, 'Classique'),
(10, 'Rap'),
(11, 'Oriental'),
(12, 'Chanson française'),
(13, 'Ataba'),
(14, 'Traditionnel'),
(15, 'Autre');

--
-- Table structure for table `album`
--
DROP TABLE IF EXISTS album;
CREATE TABLE IF NOT EXISTS `album` (
  `id_album` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL unique,
  `date_album` date DEFAULT NULL,
  `id_genre` int(11) NOT NULL,
  `pht_couvt` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_album`),
  KEY fk_genre_alb (id_genre),
  CONSTRAINT fk_genre_alb FOREIGN KEY (id_genre) REFERENCES genre (id_genre)  
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

INSERT INTO `album` (`id_album`, `nom`, `date_album`, `id_genre`, `pht_couvt`) VALUES
(1, 'Battleground', '2011-11-04', 1,'batt1.jpeg'),
(2, 'Love Life', '1984-03-12',1, 'Berlin_Love_Life.png'),
(3, 'Falling into You', '2007-10-29', 1, 'fallingintoyou.jpg'),
(4, 'Plus bleu','2010-01-01', 12, 'Plusbleu.jpeg'),
(5, 'Enrique', '1999-11-23', 1, 'enr122.png'),
(6, 'One World, One Flame', '2010-02-02', 6,'onew.jpg'),
(7, 'Believe','2012-06-15', 1, 'believe.jpg'),
(8, 'Colors','2017-06-02',1,'colors.jpg');


--
-- Table structure for table `artist`
--
DROP TABLE IF EXISTS artiste;
CREATE TABLE IF NOT EXISTS `artiste` (
  `id_artiste` int(11) NOT NULL AUTO_INCREMENT,
  `nom_artiste` varchar(45) NOT NULL unique,
  `role` varchar(45) NOT NULL,
  `pht_artiste` VARCHAR(100) NULL DEFAULT NULL,
  `pays_artiste` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id_artiste`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `artist`
--

INSERT INTO `artiste` (`id_artiste`, `nom_artiste`, `role`,`pht_artiste`,`pays_artiste`) VALUES
(1, 'Enrique Iglesias', 'chanteur','enrique.png','Canada'),
(2, 'Charles Aznavour', 'chanteur','charles.jpg','France'),
(3, 'Celine', 'chanteur','celine.jpg','Canada');
INSERT INTO `artiste` (`id_artiste`, `nom_artiste`, `role`) VALUES
(4,'Justin Bieber','chanteur'),
(5,'Danny Joe Brown', 'interprète'),
(6,'Max Pantera', 'interprète'),
(7,'Jack Yellen', 'chanteur'),
(8,'Galdston, Goldman', 'auteur'),
(9,'Bryan Adams', 'chanteur'),
(10,'Barry, Taylor', 'auteur'),
(11,'David Diamond', 'auteur'),
(12,'Beck', 'chanteur');

--
-- Table structure for table `oeuvre`
--
DROP TABLE IF EXISTS oeuvre;
CREATE TABLE IF NOT EXISTS `oeuvre` (
  `id_oeuvre` int(11) NOT NULL AUTO_INCREMENT,
  `titre_oeuvre` varchar(100) NOT NULL,
  `id_artiste` int(11) NOT NULL, 
  `dureesec` int(11) NULL,
  `taillemb` float NULL,
  `lyrics` longtext,
  `date_ajout` DATETIME DEFAULT CURRENT_TIMESTAMP,
  `id_album` int(11) NOT NULL,
  PRIMARY KEY (`id_oeuvre`),
  UNIQUE KEY `id_oeuvre` (`id_oeuvre`),
  KEY fk_oeuvre_alb (id_album),
  CONSTRAINT fk_oeuvre_alb FOREIGN KEY (id_album) REFERENCES Album (id_album),
  KEY fk_artiste_alb (id_artiste),
  CONSTRAINT fk_artiste_alb FOREIGN KEY (id_artiste) REFERENCES artiste (id_artiste)  
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

INSERT INTO oeuvre (id_oeuvre,titre_oeuvre,id_artiste, dureesec,taillemb,id_album,date_ajout) VALUES
(2,'Gitana gitana',2,153,1.6,4,'2015-04-02'),
(3,'Dis-moi que tu m''aimes',2,162,1.01,4,'2015-04-03'),
(4,'Le droit des femmes',2,267,1.85,4,'2015-04-04'),
(5,'Avant de t''aimer',6,229,1.3,4,'2015-04-05'),
(6,'Les Caraïbes',2,295,1.6,4,'2015-04-06'),
(7,'Amour amer',2,166,0.8,4,'2015-04-07'),
(8,'Au piano bar',2,289,1.9,4,'2015-04-08'),
(9,'Les images de la vie',2,270,1.2,4,'2015-04-09'),
(10,'La Yiddishe mama',7,202,1,4,'2015-04-10'),
(11,'De déraison en déraison',2,197,1.1,4,'2015-04-11'),
(12,'Ma dernière chanson pour toi',2,192,1.02,4,'2015-04-12'),
(13,'Les enfants',2,146,0.92,4,'2015-04-01'),
(14,'Nous nous reverrons un jour ou l''autre',2,224,1.1,4,'2015-04-19'),
(15,'Ce métier',2,240,1.8,4,'2015-04-10'),
(16,'It''s All Coming Back to Me Now',3,457,4.15,3,'2010-05-12'),
(17,'Because You Loved Me',3,273,2.48,3,'2010-05-17'),
(18,'Falling into You',3,258,2.35,3,'2010-05-14'),
(19,'Make You Happy',3,271,2.46,3,'2010-05-31'),
(20,'Seduces Me',3,226,2.05,3,'2010-06-06'),
(21,'All by Myself',3,312,2.84,3,'2010-05-19'),
(22,'Declaration of Love',3,260,2.36,3,'2010-06-07'),
(23,'Dreamin'' of You',3,307,2.79,3,'2010-05-27'),
(24,'I Love You',3,330,3.1,3,'2010-06-07'),
(25,'If That''s What It Takes',8,252,2.29,3,'2010-05-22'),
(26,'I Don''t Know',8,278,2.53,3,'2010-05-27'),
(27,'River Deep, Mountain High',3,250,2.27,3,'2010-05-21'),
(28,'Call the Man',3,368,3.35,3,'2010-06-07'),
(29,'Fly',8,178,1.62,3,'2010-05-25'),
(30,'One World, One Flame',9,148,1.2,6,'2017-03-04'),
(31,'All around the world (featuring Ludacris)',4,244,2.22,7,'2014-06-06'),
(32,'Boyfriend',4,171,1.55,7,'2014-06-13'),
(33,'As Long as You Love Me (featuring Big Sean)',4,229,2.08,7,'2014-06-21'),
(34,'Catching Feelings',4,234,2.13,7,'2014-06-20'),
(35,'Take You',4,220,2,7,'2014-06-07'),
(36,'Right Here (featuring Drake)',4,204,1.85,7,'2014-06-16'),
(37,'Fall',4,248,2.25,7,'2014-06-14'),
(38,'Die in Your Arms',4,237,2.15,7,'2014-06-28'),
(39,'Thought of You',4,230,2.09,7,'2014-06-27'),
(40,'Beauty and a Beat (featuring Nicki Minaj)',4,228,2.07,7,'2014-06-22'),
(41,'One Love',4,234,2.13,7,'2014-07-04'),
(42,'Be Alright',4,189,1.72,7,'2014-06-14'),
(43,'Believe',4,222,2.02,7,'2014-06-09'),
(44,'Love Me Like You Do',4,279,2.54,7,'2014-06-18'),
(45,'Fairytale',4,212,1.93,7,'2014-07-02'),
(46,'Out Of Town Girl',4,213,1.94,7,'2014-07-04'),
(47,'She Don''t Like the Lights',4,239,2.17,7,'2014-07-05'),
(48,'Maria',4,248,2.25,7,'2014-06-26'),
(49,'Just Like Them',4,172,1.56,7,'2014-07-04'),
(51,'Rhythm Divine',1,209,1.9,5,'2013-07-05'),
(52,'Be With You',10,220,2,5,'2013-07-17'),
(53,'I Have Always Loved You',1,264,2.4,5,'2013-08-04'),
(54,'Sad Eyes',1,248,2.25,5,'2013-08-04'),
(55,'I''m Your Man',1,283,2.57,5,'2013-07-11'),
(56,'Óyeme ("Listen")',1,262,2.38,5,'2013-07-09'),
(57,'Could I Have This Kiss Forever (en duo avec Whitney Houston)',1,261,2.37,5,'2013-08-04'),
(58,'You''re My #1',1,269,2.45,5,'2013-07-22'),
(59,'Alabao',1,240,2.18,5,'2013-07-14'),
(60,'Bailamos',10,214,1.95,5,'2013-07-15'),
(61,'Ritmo total (Rhythm Divine)',10,209,1.9,5,'2013-07-31'),
(62,'Más es amar (Sad Eyes)',1,254,2.31,5,'2013-07-26'),
(63,'No puedo más sin ti (I''m Your Man)',1,288,2.62,5,'2013-07-16'),
(64,'Colors',12,260,4.2,8,'2018-07-15'),
(65,'Seventh Heaven',12,300,5.4,8,'2018-07-16'),
(66,'I''m So Free',12,247,4.02,8,'2018-09-15'),
(67,'Dear Life',12,204,3.4,8,'2018-09-16'),
(68,'No Distraction',12,290,4.7,8,'2018-07-15'),
(69,'Dreams',12,295,5.4,8,'2018-07-16'),
(70,'WOW',12,220,3.12,8,'2018-09-15'),
(71,'Up All Night',12,190,3.4,8,'2018-09-16'),
(72,'Square One',12,175,2.12,8,'2018-09-15'),
(73,'Fix Me',12,193,3.1,8,'2018-09-16');


DROP TABLE IF EXISTS utilisateur;
CREATE TABLE utilisateur (
  id_utilisateur int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nom varchar(45) NOT NULL,
  courriel varchar(100) NOT NULL UNIQUE,
  mot_passe varchar(75) NOT NULL,
  age int NULL
);
/* Ceci est pour vous montrer les mots de passe avant le hashage.
INSERT INTO utilisateur (id_utilisateur, nom, mot_passe, courriel, age) VALUES
(1, 'ali', '123456', 'awdeali@gmail.com',44),
(2, 'jacob', 'asdfgh', 'jacob@gmail.com',30),
(3, 'mathieu', 'qwerty','math@hotmail.com',24),
(4, 'france', 'azerty','france@gmail.com',41),
(5, 'admin', 'admin1','admin@yahoo.ca',27);*/
INSERT INTO utilisateur (id_utilisateur, nom, mot_passe, courriel, age) VALUES
(1, 'ali',    '$2y$10$r/UN9n5f68nHEj.cmrKYZedVkkVwb.bPqglxbeUp3QWYegOqgd72q', 'awdeali@gmail.com',44),
(2, 'jacob',  '$2y$10$LTxTJNoE2rZ8FLsxN1q56Op0iT4WSyYWfKB0dVk7xeprAdPZIduP.', 'jacob@gmail.com',30),
(3, 'mathieu','$2y$10$3TnfMB4qvXQiSafeCSzakeNfbhqu3mff8y1WHbUw17FgAYvEuI1WS','math@hotmail.com',24),
(4, 'france', '$2y$10$5Y/OCXWajOXCIkj7WDgy7u4Fznu3BqaL4BlWn4FZzBiR1AEIVSFba','france@gmail.com',41),
(5, 'admin', '$2y$10$35vgM52yIeHBTkJefCXTSeF/.D.pwspopyx9YtI3hJI/MHv9iYgJ6','admin@yahoo.ca',27);
SELECT * FROM utilisateur;

DROP TABLE IF EXISTS emprunts;
CREATE TABLE emprunts (
id_utilisateur int(11) NOT NULL , 
id_oeuvre int(11) NOT NULL ,  
date_emprunt DATETIME DEFAULT CURRENT_TIMESTAMP,
date_retour DATETIME DEFAULT null,
PRIMARY KEY (id_utilisateur, id_oeuvre,date_emprunt), 
KEY fk_tbl_emprunts_tbl_utilisateur (id_utilisateur),
CONSTRAINT fk_tbl_emprunts_tbl_utilisateur FOREIGN KEY (id_utilisateur) REFERENCES utilisateur (id_utilisateur) ON DELETE NO ACTION ON UPDATE NO ACTION,
KEY fk_tbl_emprunts_tbl_oeuvre (id_oeuvre),
CONSTRAINT fk_tbl_emprunts_tbl_oeuvre FOREIGN KEY (id_oeuvre) REFERENCES oeuvre (id_oeuvre) ON DELETE NO ACTION ON UPDATE NO ACTION
);

INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('1', '2', '2020-04-01', '2020-05-01');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('2', '6', '2020-04-15', '2020-04-26');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('1', '8', '2020-07-24', '2020-08-18');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('5', '2', '2020-05-12', '2020-06-01');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('2', '4', '2020-08-10', '2020-09-02');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('4', '6', '2021-01-07', '2021-02-04');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('1', '9', '2020-11-21', '2020-12-04');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('2', '10', '2020-04-03', '2020-04-18');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('1', '25', '2020-09-18', '2020-09-28');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('5', '24', '2021-01-05', '2021-01-20');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('5', '38', '2020-12-15', '2021-01-10');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('4', '25', '2020-06-23', '2020-07-09');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('1', '24', '2020-09-24', '2020-10-23');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('2', '28', '2020-08-13', '2020-09-03');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('1', '7', '2020-10-02', '2020-10-21');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('5', '6', '2020-05-07', '2020-05-24');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('3', '57', '2020-05-08', '2020-05-31');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('4', '53', '2020-11-17', '2020-12-17');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('1', '25', '2021-01-19', '2021-02-05');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('2', '24', '2020-06-07', '2020-06-27');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('1', '15', '2020-05-16', '2020-06-04');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('5', '17', '2021-01-05', '2021-01-25');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('3', '39', '2020-08-22', '2020-09-18');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('4', '48', '2020-05-11', '2020-06-08');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('1', '41', '2020-06-22', '2020-07-16');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('2', '30', '2020-12-04', '2020-12-21');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('1', '25', '2020-06-29', '2020-07-09');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('5', '29', '2020-06-28', '2020-07-22');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('4', '27', '2020-08-03', '2020-09-01');
INSERT INTO `media_db`.`emprunts` (`id_utilisateur`, `id_oeuvre`, `date_emprunt`, `date_retour`) VALUES ('4', '28', '2020-06-08', '2020-06-28');
INSERT INTO emprunts (`id_utilisateur`, `id_oeuvre`, `date_emprunt`) 
VALUES ('2', '25', '2021-01-07'),
('1', '15', '2021-02-16'),
('1', '17', '2021-07-16'),
('2', '44', '2021-01-07'),
('1', '35', '2021-03-16'),
('3', '24', '2021-01-17'),
('4', '15', '2021-02-19'),
('2', '24', '2021-01-27'),
('4', '55', '2021-02-06'),
('4', '24', '2021-05-07'),
('3', '25', '2021-02-11'),
('3', '34', '2021-06-07'),
('2', '14', '2021-05-17'),
('3', '05', '2021-04-06'),
('4', '05', '2021-07-06');
SELECT * FROM emprunts ORDER BY id_utilisateur, id_oeuvre,date_emprunt;
SELECT nom, titre_oeuvre, date_emprunt, date_retour
FROM emprunts e JOIN oeuvre o ON e.id_oeuvre=o.id_oeuvre
JOIN utilisateur u ON u.id_utilisateur=e.id_utilisateur
ORDER BY nom,date_emprunt, titre_oeuvre ;